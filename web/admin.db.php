<?php

define('GOCATALYZE_SYNCCENTER_APPLICATION_DONT_RUN', true);

$app = require_once __DIR__ . '/index.php';
$config = $app->variablesGet();
session_start();

define('CONFIG_DRIVER', $config['database']['default']['driver']);
define('CONFIG_HOST', isset($config['database']['default']['host']) ? $config['database']['default']['host'] : null);
define('CONFIG_DB', isset($config['database']['default']['dbname']) ? $config['database']['default']['dbname'] : (isset($config['database']['default']['path']) ? $config['database']['default']['path'] : null));
define('CONFIG_USER', $config['database']['default']['user']);
define('CONFIG_PASSWORD', $config['database']['default']['password']);

if (!empty($config['basic_auth'])) {
    define('ADMIN_USER', $config['basic_auth']['username']);
    define('ADMIN_PASSWORD', $config['basic_auth']['password']);
}
else {
    throw new Exception('Please config credentials for admin.');
}

function adminer_object()
{

    class AdminerSoftware extends Adminer
    {

        function name()
        {
            return 'GC Sync';
        }

        function credentials()
        {
            return [CONFIG_HOST, CONFIG_USER, CONFIG_PASSWORD];
        }

        function database()
        {
            return CONFIG_DB;
        }

        function login($login, $password)
        {
            return ($login === ADMIN_USER) && ($password === ADMIN_PASSWORD);
        }

    }

    return new AdminerSoftware();
}

if (!empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])) {
    $_GET['db'] = CONFIG_DB;
    $_GET['username'] = ADMIN_USER;

    if (ADMIN_USER === $_SERVER['PHP_AUTH_USER'] && ADMIN_PASSWORD === $_SERVER['PHP_AUTH_PW']) {
        if (empty($_SESSION['atadmin']) && empty($_POST['auth'])) {
            $_SESSION['atadmin'] = true;
            $_POST['auth'] = [
                'driver'   => str_replace('pdo_', '', CONFIG_DRIVER),
                'username' => ADMIN_USER,
                'password' => ADMIN_PASSWORD,
            ];
        }
    }

    if (!empty($_POST['logout'])) {
        unset($_SESSION['atadmin']);
    }
}

require __DIR__ . "/../files/adminer.php";
