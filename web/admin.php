<?php
// quite simple, …
define('GOCATALYZE_SYNCCENTER_APPLICATION_DONT_RUN', true);
require_once __DIR__ . '/index.php';
?><!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" ng-app="SyncUI">
    <head>
        <meta charset="UTF-8" />
        <title>GoCatalyze Sync</title>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.17/angular.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.17/angular-resource.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.17/angular-route.min.js"></script>
        <script src="./js/contrib/ui-bootstrap-tpls-0.11.0.min.js"></script>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
        <style>
            .form-inline .form-group input.form-control { margin-right: 10px; }
        </style>

        <script src="/js/services.js?<?= time() ?>"></script>
        <script src="/js/routes.js?<?= time() ?>"></script>
        <script src="/js/directives.js?<?= time() ?>"></script>
        <script src="/js/ctrl.mapping.queue.js?<?= time() ?>"></script>
        <script src="/js/ctrl.instance.edit.js?<?= time() ?>"></script>
        <script src="/js/ctrl.mapping.edit.js?<?= time() ?>"></script>
        <script src="/js/app.js?<?= time() ?>"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">GoCatalyze Sync</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#/mapping">Mappings</a></li>
                        <li><a href="#/instance">Instances</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div>

        <div class="container">
            <div ng-view></div>
        </div>

        <div class="footer">
            <div class="container">
                <p class="text-muted"><!-- Footer --></p>
            </div>
        </div>
    </body>
</html>
