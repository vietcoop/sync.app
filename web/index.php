<?php

use GoCatalyze\SyncApp\Application;

// ---------------------
// Auto class loading
// ---------------------
$locations[] = __DIR__ . "/../vendor/autoload.php";
$locations[] = __DIR__ . "/../../../autoload.php";

foreach ($locations as $location) {
    if (is_file($location)) {
        require_once $location;
    }
}

// ---------------------
// Run application
// ---------------------
$application = new Application(dirname(__DIR__));

if (!defined('GOCATALYZE_SYNCCENTER_APPLICATION_DONT_RUN')) {
    $application->getRoute()->handle();
}
else {
    return $application;
}
