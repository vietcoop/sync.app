(function(angular) {
    var ctrl = function($scope, $location, $routeParams, AppInfo, Instances, RemoteEntityFields, Mappings) {
        $scope.info = AppInfo.get();
        $scope.instances = Instances.query();
        $scope._source = $scope._destination = {};
        $scope.priorities = [
            {priority: -5, label: "Low"},
            {priority: 0, label: "Normal"},
            {priority: 5, label: "High"}
        ];

        $scope.mapping = {priority: 0, status: true, destination: {unique_fields: [['']]}, mapping: [{source: [['']], destination: ''}]};
        if (typeof $routeParams.id !== 'undefined') {
            $scope.mapping = Mappings.get({id: $routeParams.id}, function() {
                // multiple fields for source, like: source = first + last => full name
                for (var index in $scope.mapping.mapping) {
                    if (typeof $scope.mapping.mapping[index].source === 'string')
                        $scope.mapping.mapping[index].source = [[$scope.mapping.mapping[index].source]];

                    for (var row_i in $scope.mapping.mapping[index].source)
                        if (typeof $scope.mapping.mapping[index].source[row_i] === 'string')
                            $scope.mapping.mapping[index].source[row_i] = [$scope.mapping.mapping[index].source[row_i]];
                }

                // a blank line for field mapping
                $scope.mapping.mapping.push({'source': [['']], 'destination': ''});

                // fetch entity/field info
                $scope.selectedSourceService('source');
                $scope.selectedSourceService('destination');
                $scope.selectedSourceRemoteEntityType('source');
                $scope.selectedSourceRemoteEntityType('destination');

                // Provide unique fields input if it's empty
                if (typeof $scope.mapping.destination.unique_fields === 'undefined')
                    $scope.mapping.destination.unique_fields = [['']];

                // convert legacy structure to new ['…'] => [ ['…'] ]
                if ($scope.mapping.destination.unique_fields instanceof Array)
                    if (typeof $scope.mapping.destination.unique_fields[0] === 'string')
                        $scope.mapping.destination.unique_fields = [$scope.mapping.destination.unique_fields];
            });
        }

        $scope.selectedSourceService = function(side) {
            $scope['_' + side] = Instances.get({id: $scope.mapping[side].service_instance.id});
        };

        $scope.selectedSourceRemoteEntityType = function(side) {
            $scope['_' + side + '_fields'] = RemoteEntityFields.get({
                id: $scope.mapping[side].service_instance.id,
                entity_type: $scope.mapping[side].remote_entity_type
            });
        };

        $scope.add_field_mapping = function() {
            var _size = $scope.mapping.mapping.length;
            var _last = $scope.mapping.mapping[_size - 1];
            if (_last.source && _last.destination)
                $scope.mapping.mapping.push({'source': [['']], 'destination': ''});
        };

        $scope.addSourceField = function(row, sub_row) {
            var _last = $scope.mapping.mapping[row].source[sub_row].length - 1;
            if ($scope.mapping.mapping[row].source[sub_row][_last])
                $scope.mapping.mapping[row].source[sub_row].push('');
        };

        $scope.addSourceRow = function(row) {
            $scope.mapping.mapping[row].source.push(['']);
        };

        $scope.addDestinationUniqueFields = function(i) {
            var _last = $scope.mapping.destination.unique_fields[i].length - 1;
            if ($scope.mapping.destination.unique_fields[i][_last])
                $scope.mapping.destination.unique_fields[i].push('');
        };

        $scope.addDestinationUniqueFieldsRow = function() {
            $scope.mapping.destination.unique_fields.push(['']);
        };

        $scope.cancel = function() {
            $location.path('/mapping');
        };

        $scope.save = function() {
            doSave($location, $scope, Mappings, JSON.parse(JSON.stringify($scope.mapping)));
        };
    };

    var doSave = function($location, $scope, Mappings, mapping) {
        var params = {
            id: mapping.id,
            status: mapping.status,
            priority: mapping.priority,
            description: mapping.description,
            source: {
                service_instance: mapping.source.service_instance.id,
                remote_entity_type: mapping.source.remote_entity_type,
                entity_type: mapping.source.entity_type
            },
            destination: {
                service_instance: mapping.destination.service_instance.id,
                remote_entity_type: mapping.destination.remote_entity_type,
                entity_type: mapping.destination.entity_type,
                unique_fields: mapping.destination.unique_fields
            },
            mapping: []
        };

        for (var i in mapping.mapping) {
            if (mapping.mapping[i].source && mapping.mapping[i].destination) {
                params.mapping.push({source: mapping.mapping[i].source, destination: mapping.mapping[i].destination});
            }

            if (typeof params.mapping[i] !== 'undefined') {
                for (var ii in params.mapping[i].source) {
                    delete params.mapping[i].source[ii]['$$hashKey'];
                    var row = params.mapping[i].source[ii].length == 1
                            ? params.mapping[i].source[ii][0]
                            : params.mapping[i].source[ii];
                    params.mapping[i].source[ii] = row;
                }

                if (1 == params.mapping[i].source.length)
                    if ('string' === typeof params.mapping[i].source[0])
                        params.mapping[i].source = params.mapping[i].source[0];
            }
        }

        for (var i in params.destination.unique_fields)
            for (var ii in params.destination.unique_fields[i])
                if (!params.destination.unique_fields[i][ii])
                    delete(params.destination.unique_fields[i][ii]);

        mapping.id
                ? Mappings.update(params,
                        function() {
                            $location.path('/mapping');
                        },
                        function(error) {
                            $scope.error = error;
                        })
                : Mappings.create(params, function() {
                    $location.path('/mapping');
                });
    };

    angular
            .module('SyncMappingCtrlEdit', ['ngRoute'])
            .controller(
                    'MappingEditCtrl',
                    ['$scope', '$location', '$routeParams', 'AppInfo', 'Instances', 'RemoteEntityFields', 'Mappings', ctrl])
            ;
})(angular);
