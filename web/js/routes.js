(function(angular) {
    angular
            .module('SyncRoutes', ['ngRoute', 'SyncInstanceCtrlEdit', 'SyncMappingCtrlEdit', 'SyncMappingCtrlLog'])
            .controller('InstanceListCtrl', ['$scope', '$route', 'baseURL', 'Instances', function($scope, $route, baseURL, Instances) {
                    $scope.instances = Instances.query();

                    // export callback
                    $scope.exportInstance = function(id) {
                        window.open(baseURL + '/instance/' + id + '.json?export=1');
                    };

                    // delete callback
                    $scope.deleteInstance = function(id) {
                        if (confirm('Are you sure to delete the service instance?')) {
                            Instances.delete({id: id}, function() {
                                $route.reload();
                            });
                        }
                    };
                }])
            .controller('MappingListCtrl', ['$scope', 'baseURL', 'Mappings', function($scope, baseURL, Mappings) {
                    $scope.mappings = Mappings.query();

                    // export callback
                    $scope.exportmapping = function(id) {
                        window.open(baseURL + '/mapping/' + id + '.json?export=1');
                    };

                    // delete callback
                    $scope.deleteMapping = function(id) {
                        if (confirm('Are you sure to delete the mapping?')) {
                            Mappings.delete({id: id}, function() {
                                for (var i in $scope.mappings)
                                    if (id === $scope.mappings[i].id)
                                        delete($scope.mappings[i]);
                            });
                        }
                    };
                }])
            .config(function($routeProvider) {
                $routeProvider
                        .when('/instance', {
                            controller: 'InstanceListCtrl',
                            templateUrl: '/templates/instances.html'
                        })
                        .when('/instance/:id/edit', {
                            controller: 'InstanceEditCtrl',
                            templateUrl: '/templates/instance.edit.html'
                        })
                        .when('/instance/new', {
                            controller: 'InstanceEditCtrl',
                            templateUrl: '/templates/instance.edit.html'
                        })
                        .when('/mapping', {
                            controller: 'MappingListCtrl',
                            templateUrl: '/templates/mappings.html'
                        })
                        .when('/mapping/new', {
                            controller: 'MappingEditCtrl',
                            templateUrl: '/templates/mapping.edit.html'
                        })
                        .when('/mapping/:id/edit', {
                            controller: 'MappingEditCtrl',
                            templateUrl: '/templates/mapping.edit.html'
                        })
                        .when('/mapping/:id/queue', {
                            controller: 'QueueJobCtrl',
                            templateUrl: '/templates/mapping.queue.html'
                        })
                        .otherwise({redirectTo: '/mapping'});
            });
})(angular);
