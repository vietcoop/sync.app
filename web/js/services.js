(function(angular) {
    angular
            .module('SyncServices', ['ngRoute', 'ngResource'])
            .value('baseURL', '/index.php')
            .factory('AppInfo', ['$resource', 'baseURL', function($resource, baseURL) {
                    return $resource(baseURL + '/info.json');
                }])
            .factory('Instances', ['$resource', 'baseURL', function($resource, baseURL) {
                    return $resource(baseURL + '/instance/:id.json', {id: '@id'}, {
                        get: {method: 'GET'},
                        query: {method: 'GET', params: {id: 'all'}, isArray: true},
                        update: {method: 'PUT', params: {id: '@id'}},
                        create: {method: 'POST'}
                    });
                }])
            .factory('RemoteEntityFields', ['$resource', 'baseURL', function($resource, baseURL) {
                    return $resource(baseURL + '/instance/:id/info/:entity_type/fields.json', {}, {});
                }])
            .factory('Mappings', ['$resource', 'baseURL', function($resource, baseURL) {
                    return $resource(baseURL + '/mapping/:id.json', {id: '@id'}, {
                        get: {method: 'GET'},
                        query: {method: 'GET', params: {id: null}, isArray: true},
                        update: {method: 'PUT', params: {id: '@id'}},
                        create: {method: 'POST'}
                    });
                }])
            .factory('Queue', ['$resource', 'baseURL', function($resource, baseURL) {
                    return $resource(baseURL + '/mapping/:id/queue/:job_id.json', null, {
                        query: {method: 'GET', params: {id: '@id', job_id: null}},
                        getJob: {method: 'GET'}
                    });
                }]);
    ;
})(angular);
