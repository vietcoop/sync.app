(function(angular) {

    var ctrl = function($scope, $route, $routeParams, $resource, baseURL, Mappings) {
        // Resources
        var sourceFetcher = $resource(baseURL + '/mapping/:id/fetch', {id: '@id'});
        var jobResource = $resource(baseURL + '/queue/process/:id');
        var jobLoader = $resource(baseURL + '/mapping/:id/queue/:job_id', {}, {
            query: {method: 'GET', params: {id: '@id', job_id: null}}
        });

        $scope.loadJobs = function() {
            var page = $scope.pageCurrent;
            var limit = 10;
            var offset = limit * (page - 1);
            jobLoader.query({id: $routeParams.id, limit: limit, offset: offset}, function(data) {
                $scope.jobs = data.results;

                // pagination
                $scope.pageTotalItems = data.total;
                $scope.pageCurrent = page;
                $scope.pageMax = 12;
                $scope.pageNums = 1 + data.total / data.limit;

                // tobe removed
                $scope.pages = [];
                for (var i = 1; i < 1 + data.total / data.limit; i++)
                    $scope.pages.push(i);
            });
        };

        $scope.fetchSourceEntities = function() {
            $scope._fetching = true;
            sourceFetcher.get({id: $routeParams.id}, function(data) {
                $scope._fetching = false;
                $scope._fetch_status = 'Fetched items: ' + data.entity_number + '. Runtime: ' + data.runtime;
                if (0 < data.entity_number)
                    $route.reload();
            });
        };

        $scope.hideSourceEntitiesStatus = function() {
            delete($scope._fetch_status);
        };

        $scope.jobLoad = function(job_id) {
            jobLoader.get({id: $routeParams.id, job_id: job_id}, function(data) {
                $scope._job = data;
            });
        };

        $scope.jobExecute = function(job_id) {
            $scope._job_executing = true;
            jobResource.get({id: job_id}, function(data) {
                $scope._job_executing = false;
                $route.reload();
                $scope._job = data;
            });
        };

        $scope.jobDelete = function(job_id) {
            jobLoader.delete({id: $routeParams.id, job_id: job_id}, function() {
                $route.reload();
            });
        };

        $scope.mapping = Mappings.get({id: $routeParams.id});
        $scope.loadJobs(1);
        $scope._job; // context job
    };

    angular
            .module('SyncMappingCtrlLog', ['ngRoute', 'ngResource', 'ui.bootstrap'])
            .controller(
                    'QueueJobCtrl',
                    ['$scope', '$route', '$routeParams', '$resource', 'baseURL', 'Mappings', ctrl])
            ;

})(angular);
