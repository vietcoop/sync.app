(function(angular) {
    var ctrl = function($scope, $location, $routeParams, AppInfo, Instances) {
        $scope.info = AppInfo.get();
        $scope.instance = {options: {}};

        // Load entity, convert it to easy template style.
        if ($routeParams.id) {
            $scope.instance = Instances.get({id: $routeParams.id}, function() {
                if (typeof $scope.instance.options.endpoints === undefined) {
                    return;
                }

                if ($scope.instance.options instanceof Array) {
                    $scope.instance.options = {};
                }

                var endpoints = [];
                for (var entity_type in $scope.instance.options.endpoints) {
                    for (var http_method in $scope.instance.options.endpoints[entity_type]) {
                        var entity_endpoint = $scope.instance.options.endpoints[entity_type][http_method];
                        endpoints.push([entity_type, http_method, entity_endpoint]);
                    }
                }

                endpoints.push(['', '', '']);

                $scope.instance.options.endpoints = endpoints;
            });
        }

        // Add more optiosn
        $scope.endpoint_more = function() {
            var _size = $scope.instance.options.endpoints.length;
            var _last = $scope.instance.options.endpoints[_size - 1];
            if (_last[0] && _last[1] && _last[2]) {
                $scope.instance.options.endpoints.push(['', '', '']);
            }
        };

        $scope.cancel = function() {
            $location.path('/instances');
        };

        $scope.save = function() {
            doSave($scope, Instances, $location, JSON.parse(JSON.stringify($scope.instance)));
        };
    };

    var doSave = function($scope, Instances, $location, instance) {
        var record = {
            description: instance.description,
            service_name: instance.service_name,
            options: instance.options
        };

        if (typeof instance.id !== 'undefined') {
            record.id = instance.id;
        }

        // Convert endpoints to correct format
        if (typeof record.options.endpoints !== 'undefined') {
            var endpoints = {};
            for (var i in record.options.endpoints) {
                var entity_type = record.options.endpoints[i][0];
                var http_method = record.options.endpoints[i][1];
                var entity_endpoint = record.options.endpoints[i][2];
                if (entity_type && http_method && entity_endpoint) {
                    endpoints[entity_type] = endpoints[entity_type] || {};
                    endpoints[entity_type][http_method] = entity_endpoint;
                }
            }

            record.options.endpoints = endpoints;
        }

        delete($scope.error);
        $scope._instance_saving = true;

        if (typeof record.id === 'undefined') {
            Instances.create(record,
                    function(data) {
                        $location.path('/instance/' + data.id + '/edit');
                        $scope._instance_saving = false;
                    },
                    function(error) {
                        $scope.error = error;
                        $scope._instance_saving = false;
                    }
            );
        }
        else {
            Instances.update(record,
                    function(data) {
                        $location.path('/instances');
                        $scope._instance_saving = false;
                    },
                    function(error) {
                        $scope.error = error;
                        $scope._instance_saving = false;
                    }
            );
        }
    };

    angular
            .module('SyncInstanceCtrlEdit', ['SyncServices', 'SyncDirectives'])
            .controller(
                    'InstanceEditCtrl',
                    ['$scope', '$location', '$routeParams', 'AppInfo', 'Instances', ctrl])
            ;
})(angular);
