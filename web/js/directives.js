(function(angular) {
    angular
            .module('SyncDirectives', [])
            .directive('configSchema', function() {
                return {
                    restrict: 'E',
                    templateUrl: '/templates/instance.edit.config-schema.html'
                };
            });
})(angular);
