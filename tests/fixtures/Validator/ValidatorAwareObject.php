<?php

namespace GoCatalyze\SyncApp\Testing\Fixtures\Validator;

use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class ValidatorAwareObject
{

    /**
     * @var string
     */
    private $name;

    /**
     * Setter for name property.
     * 
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new NotNull());
    }

}
