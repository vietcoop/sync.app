<?php

namespace GoCatalyze\SyncApp\Testing\Services;

use GoCatalyze\SyncApp\Testing\BaseTestCase;
use GoCatalyze\SyncApp\Testing\Fixtures\Validator\ValidatorAwareObject;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ValidatorTest extends BaseTestCase
{

    /**
     * @dataProvider insideValidatorDataProvider
     */
    public function testInsideValidator($obj, $count)
    {
        $error = $this->app->getValidator()->validate($obj);
        $this->assertInstanceOf('Symfony\Component\Validator\ConstraintViolationList', $error);
        $this->assertCount($count, $error);
    }

    public function insideValidatorDataProvider()
    {
        $obj1 = new ValidatorAwareObject();
        $obj2 = new ValidatorAwareObject();
        $obj2->setName('OK');
        return [
            [$obj1, 1],
            [$obj2, 0],
        ];
    }

    public function testOutsideValidator()
    {
        $obj = new ValidatorAwareObject();
        $obj->setName('Four');

        $validator = $this->app->getValidator();
        $metadata = $validator->getMetadataFor($obj);
        $metadata->addPropertyConstraint('name', new Length(['min' => 5]));
        $error = $validator->validate($obj);

        $this->assertCount(1, $error);
        $this->assertEquals(
            'This value is too short. It should have 5 characters or more.', $error->get(0)->getMessage()
        );
    }

    /**
     * @group study
     */
    public function testCallback()
    {
        $is_true = function($input, ExecutionContextInterface $context) {
            if (true !== $input) {
                $context->addViolation('Invalid true value', [], [], false);
            }
        };

        $input = ['yes' => true, 'no' => false];
        $constraints = new Collection([
            'yes' => new Callback(['callback' => $is_true]),
            'no' => new Callback(['callback' => $is_true]),
        ]);

        $validator = $this->app->getValidator();
        $error = $validator->validateValue($input, $constraints);

        $this->assertCount(1, $error);
        $this->assertContains('Invalid true value', (string) $error->offsetGet(0));
    }

}
