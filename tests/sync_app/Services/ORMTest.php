<?php

namespace GoCatalyze\SyncApp\Testing\Services;

use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\Testing\BaseTestCase;

class ORMTest extends BaseTestCase
{

    /**
     * {@inheritdoc}
     */
    protected $need_db = true;

    public function testConnection()
    {
        $this->assertEquals([
            'driver'   => 'pdo_sqlite',
            'user'     => 'db_user',
            'password' => 'db_pwd',
            'path'     => $this->app->getAppRootDir() . '/files/syncapp_testing.sqlite',
            'memory'   => true,
            'charset'  => 'UTF8',
            ], $this->app->getEntitiyManager()->getConnection()->getParams());
    }

    /**
     * @dataProvider dataProviderRepository
     */
    public function testRepository($class_name)
    {
        $this->assertInstanceOf(
            'Doctrine\\ORM\\EntityRepository', $this->app->getEntitiyManager()->getRepository($class_name)
        );
    }

    public function dataProviderRepository()
    {
        return [
            ['GoCatalyze\SyncApp\Testing\Fixtures\Entity\DemoOBject'],
            ['GoCatalyze\SyncApp\Entity\ServiceInstanceEntity']
        ];
    }

    /**
     * @group QUEUE
     */
    public function testQueue()
    {
        $em = $this->app->getEntitiyManager();
        $job = new QueueJobEntity('FooCommand', ['foo' => 'value']);
        $em->persist($job);
        $em->flush();

        /* @var $saved_job QueueJobEntity */
        $saved_job = $this->app->getQueueJob($job->getId());
        $this->assertEquals($job->getId(), $saved_job->getId());
        $this->assertEquals('FooCommand', $saved_job->getCommand());
        $this->assertEquals(['foo' => 'value'], $saved_job->getArgs());

        /* @var $action_job QueueJobEntity */
        $action_job = $this->app->getQueueJob();
        $this->assertNotEmpty($action_job->getId());
        $this->assertNotEmpty($action_job->getArgs());

        // get multiples
        $results = $this->app->getQueueJobs();
        $this->assertGreaterThan(1, $results['total']);
        $this->assertEquals(50, $results['limit']);
        $this->assertEquals(0, $results['offset']);
        $this->assertGreaterThanOrEqual(1, $results['results']);
    }

}
