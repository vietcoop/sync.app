<?php

namespace GoCatalyze\SyncApp\Testing;

use GoCatalyze\SyncApp\Entity\ServiceInstanceEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity;
use PHPQueue\Job;
use DateTime;
use GoCatalyze\SyncCenter\Entity\EntityInterface;

class DrupalToSalesforceTest extends BaseTestCase
{

    /**
     * @return ServiceInstanceEntity
     */
    private function makeDrupalInstance()
    {
        $instance = ServiceInstanceEntity::fromArray([
                'service_name' => 'drupal',
                'description' => 'Test Drupal instance',
                'options' => [
                    'host' => 'http://127.0.0.1/',
                    'name' => 'admin',
                    'pass' => 'password',
                    'endpoints' => [
                        'user' => ['GET' => ['/views_user']]
                    ]
                ]
        ]);

        $em = $this->app->getEntitiyManager();
        $em->persist($instance);
        $em->flush();

        return $instance;
    }

    private function makeSalesforceInstance()
    {
        $instance = ServiceInstanceEntity::fromArray([
                'service_name' => 'salesforce',
                'description' => 'Test Salesforce instance',
                'options' => [
                    'client_id' => '***',
                    'client_secret' => '***',
                    'instance_url' => 'https://test.salesforce.com',
                    'refresh_token' => '***',
                ]
        ]);

        $em = $this->app->getEntitiyManager();
        $em->persist($instance);
        $em->flush();

        return $instance;
    }

    /**
     * @return SyncMappingEntity
     */
    private function makeSyncMapping()
    {
        $sync_mapping = SyncMappingEntity::fromArray([
                'status' => true,
                'description' => 'Drupal user to Salesforce',
                'source' => [
                    'service_instance' => $this->makeDrupalInstance(),
                    'entity_type' => 'drupal.entity',
                    'remote_entity_type' => 'user',
                ],
                'destination' => [
                    'service_instance' => $this->makeSalesforceInstance(),
                    'entity_type' => 'salesforce.entity',
                    'remote_entity_type' => 'Contact',
                    'unique_fields' => ['SYN_Id__c']
                ],
                'mapping' => [
                    ['source' => 'uid', 'destination' => 'SYN_Id__c'],
                    ['source' => 'first_name', 'destination' => 'FirstName'],
                    ['source' => 'last_name', 'destination' => 'LastName'],
                    ['source' => 'mail', 'destination' => 'Mail'],
                ]
        ]);

        $sync_mapping->setUpdatedAt(new DateTime());

        $em = $this->app->getEntitiyManager();
        $em->persist($sync_mapping);
        $em->flush();

        return $sync_mapping;
    }

    /**
     * No config-aware, hardcode a sync mapping object.
     *
     * @return Job
     */
    private function makeQueueCommand()
    {
        $queue_item = new Job();
        $queue_item->data = [
            'action' => 'create',
            'sync_mapping' => $this->makeSyncMapping()->getId(),
            'attributes' => [
                'uid' => 1,
                'first_name' => 'Andy',
                'last_name' => 'Truong',
                'mail' => 'heyandy@x.v3k.net'
            ]
        ];
        return $queue_item;
    }

    /**
     * Simple, make sure there is no issue on creating queue item.
     */
    private function checkQueueCommand()
    {
        $queue_item = $this->makeQueueCommand();

        // Add more assertions here
        // …

        return $queue_item;
    }

    private function checkSyncMappingSaving()
    {
        $queue_item = $this->checkQueueCommand();

        // load sync-mapping by ID
        /* @var $sync_mapping SyncMappingEntity */
        $sync_mapping = $this->getEntityManager()->getRepository('GoCatalyze\SyncApp\Entity\SyncMappingEntity')->find($queue_item->data['sync_mapping']);

        // check sync-mapping arguments, all should be saved correctly
        $this->assertInstanceOf('GoCatalyze\SyncApp\Entity\SyncMappingEntity', $sync_mapping);
        $this->assertEquals('drupal', $sync_mapping->getSourceServiceInstance()->getServiceName());
        $this->assertEquals('user', $sync_mapping->getSourceRemoteEntityType());
        $this->assertEquals('salesforce', $sync_mapping->getDestinationServiceInstance()->getServiceName());
        $this->assertEquals('Contact', $sync_mapping->getDestinationRemoteEntityType());
        $this->assertEquals(['SYN_Id__c'], $sync_mapping->getDestinationUniqueFields());
        $this->assertEquals(['source' => 'mail', 'destination' => 'Mail'], $sync_mapping->getMappingInfo()[3]);

        return $queue_item;
    }

    private function checkEntityCasting()
    {
        /* @var $queue_item Job */
        /* @var $mapping SyncMappingEntity */
        $queue_item = $this->checkSyncMappingSaving();
        $input = $queue_item->data['attributes'];
        $mapping = $this->getEntityManager()->getRepository('GoCatalyze\SyncApp\Entity\SyncMappingEntity')->find($queue_item->data['sync_mapping']);

        $convertor = $this->app->getSyncManager()->getEntityConvertor();
        $field_mapping = EntityMapping::fromArray($mapping->getMappingInfo());

        $drupal_entity = new DrupalEntity();
        $drupal_entity->setAttributeValues($input);

        // Convert source entity to destination entity
        $sf_entity = $convertor->convert($drupal_entity, 'salesforce.entity', $field_mapping);
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity', $sf_entity);
        $this->assertEquals([
            'SYN_Id__c' => $input['uid'],
            'FirstName' => $input['first_name'],
            'LastName' => $input['last_name'],
            'Mail' => $input['mail']
            ], $sf_entity->getAttributes()
        );

        return [$queue_item, $mapping, $sf_entity];
    }

    public function checkDestinationService()
    {
        /* @var $queue_item Job */
        /* @var $sync_mapping SyncMappingEntity */
        /* @var $sf_entity SalesforceEntity */
        list($queue_item, $sync_mapping, $sf_entity) = $this->checkEntityCasting();

        $dest_service = $this->app->getSyncManager()->getService(
            $sync_mapping->getDestinationServiceInstance()->getServiceName(), $sync_mapping->getDestinationServiceInstance()->getOptions()
        );

        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceService', $dest_service);

        return [$queue_item, $sync_mapping, $sf_entity, $dest_service];
    }

    /**
     * @group heyandy
     */
    public function testRemotePushing()
    {
        /* @var $queue_item Job */
        /* @var $sync_mapping SyncMappingEntity */
        /* @var $sf_entity SalesforceEntityyncCenter\Extensions\Salesforce\SalesforceService */
        list($queue_item, $sync_mapping, $sf_entity, $sf_service) = $this->checkDestinationService();

        $action = $queue_item->data['action'];
        $type = $sync_mapping->getDestinationRemoteEntityType();
        $uniques = $sync_mapping->getDestinationUniqueFields();

        $client = $this->getMock('GoCatalyze\SyncCenter\BaseClient');

        if ($action === 'create') {
            /* @var $full_entity EntityInterface */
            $full_entity = clone ($sf_entity);
            $full_entity->setAttributeValue('Id', 112233);
            $client->expects($this->once())
                ->method('doCreate')
                ->with($sf_entity, $type)
                ->willReturn($full_entity);
        }

        $sf_service->setClient($client);

        $response = $sf_service->{$action}($sf_entity, $type, $uniques);
        $this->assertEquals(112233, $response['Id']);
    }

}
