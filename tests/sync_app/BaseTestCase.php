<?php

namespace GoCatalyze\SyncApp\Testing;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use GoCatalyze\SyncApp\Application;
use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\Entity\ServiceInstanceEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use PHPQueue\Job;
use PHPUnit_Framework_TestCase;
use SyncCenterTestManager;

abstract class BaseTestCase extends PHPUnit_Framework_TestCase
{

    /**
     * Application
     *
     * @var Application
     */
    protected $app;

    /**
     * Override this to true if you need entity CRUD functionality in use cases.
     *
     * @var boolean
     */
    protected $need_db = false;

    public function setUp()
    {
        parent::setUp();
        $this->app = SyncCenterTestManager::getApplication();
    }

    /**
     * Get entity manager.
     *
     * @staticvar boolean $ran
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        static $ran = false;

        if (false === $ran) {
            $ran = true;

            if (true === $this->need_db) {
                $this->setupDB();
            }
        }

        return $this->app->getEntitiyManager();
    }

    private function setupDB()
    {
        // getting objects
        $em = $this->app->getEntitiyManager();
        $metadatas = $em->getMetadataFactory()->getAllMetadata();
        $schema_tool = new SchemaTool($em);

        // drop all schemas
        $schema_tool->dropSchema($metadatas);

        // recreate schemas
        $schema_tool->createSchema($metadatas);
    }

    /**
     * @return ServiceInstanceEntity
     */
    protected function dummyDrupalServiceInstance()
    {
        return ServiceInstanceEntity::fromArray([
                'service_name' => 'drupal',
                'description'  => 'A demo Drupal Instance',
                'options'      => [
                    'host'        => 'http://127.0.0.1/drupal/entity', 'token'       => 'SAMPLE TOKEN',
                    'entity_info' => [
                        'user' => ['entity keys' => ['id' => 'uid']]
                    ]
                ]
        ]);
    }

    /**
     * @return ServiceInstanceEntity
     */
    protected function dummySalesforceServiceInstance()
    {
        return ServiceInstanceEntity::fromArray([
                'service_name' => 'salesforce',
                'description'  => 'A demo Salesforce Instance',
                'options'      => [
                    'instance_url' => 'http://unreal_' . __LINE__ . '.salesforce.com/',
                    'token'        => 'MySaleforceToken',
                ],
        ]);
    }

    protected function dummySyncMapping($source, $destination)
    {
        $_source = [
            'service_instance'   => $source === 'drupal' ? $this->dummyDrupalServiceInstance() : $this->dummySalesforceServiceInstance(),
            'entity_type'        => $source === 'drupal' ? 'drupal.entity' : 'salesforce.entity',
            'remote_entity_type' => $source === 'drupal' ? 'user' : 'Contact',
        ];

        $_dest = [
            'service_instance'   => $destination === 'drupal' ? $this->dummyDrupalServiceInstance() : $this->dummySalesforceServiceInstance(),
            'entity_type'        => $destination === 'drupal' ? 'drupal.entity' : 'salesforce.entity',
            'remote_entity_type' => $destination === 'drupal' ? 'user' : 'Contact',
        ];

        $mapping = [];
        $mapping['drupal'][] = ['source' => 'uid', 'destination' => 'drupal_id__c'];
        $mapping['drupal'][] = ['source' => 'mail', 'destination' => 'Mail'];
        $mapping['salesforces'][] = ['source' => 'Id', 'destination' => 'field_salesforce_id'];
        $mapping['salesforces'][] = ['source' => 'Id', 'Mail' => 'mail'];

        return SyncMappingEntity::fromArray([
                'id'          => __LINE__,
                'status'      => true,
                'description' => 'Drupal to Saleforce mapping',
                'source'      => $_source,
                'destination' => $_dest,
                'mapping'     => $mapping[$source]
        ]);
    }

    /**
     * @param string $action
     * @param array $attributes
     * @return QueueJobEntity
     */
    protected function dummyQueueItem($source, $destination, $action, array $attributes)
    {
        $data = [
            'action'       => $action,
            'sync_mapping' => $this->dummySyncMapping($source, $destination),
            'attributes'   => $attributes
        ];

        $job = new QueueJobEntity('syncEntity', $data);
        $job->setId(__LINE__ + rand(100, 900));
        return $job;
    }

    protected function dummyDrupalInput()
    {
        $name = 'john' . (__LINE__ + rand(100, 900));
        return [
            'uid'  => __LINE__ + rand(100, 900),
            'name' => $name,
            'mail' => $name . '@english.com',
        ];
    }

    protected function dummySalesforceInput()
    {
        $number = __LINE__ + rand(1000, 9000);
        return [
            'Id'   => $number,
            'Name' => 'John English Number ' + $number,
            'Mail' => 'john' . $number . '@english.com',
        ];
    }

    protected function dummyEntityQueue($action, $service)
    {
        return [
            'action'       => $action,
            'sync_mapping' => $this->dummySyncMapping($service, $service === 'drupal' ? 'salesforce' : 'drupal'),
            'attributes'   => $service === 'drupal' ? $this->dummyDrupalInput() : $this->dummySalesforceInput(),
        ];
    }

}
