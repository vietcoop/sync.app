<?php

namespace GoCatalyze\SyncApp\Testing\Controller;

use GoCatalyze\SyncApp\Controller\ServiceInstaceController;
use GoCatalyze\SyncApp\Controller\MappingController;
use GoCatalyze\SyncApp\Testing\BaseTestCase;

class MappingControllerTest extends BaseTestCase
{

    private function createDrupalInstance()
    {
        $data = [
            'service_name' => 'drupal',
            'description' => 'A demo Drupal Instance',
            'options' => [
                'host' => 'http://127.0.0.1/drupal',
            ],
            'entity_info' => [
                'node' => ['entity keys' => ['id' => 'nid']],
                'user' => ['entity keys' => ['id' => 'uid']],
            ]
        ];

        $controller = new ServiceInstaceController($this->app);
        $response = $controller->post($data);

        $this->assertNotEmpty($response['id']);

        return $response['id'];
    }

    private function createSaleforceInstance()
    {
        $data = [
            'service_name' => 'salesforce',
            'description' => 'A demo Salesforce Instance',
            'options' => [
                'token' => 'MySaleforceToken',
            ],
            'entity_info' => [
                'Contact' => []
            ]
        ];

        $controller = new ServiceInstaceController($this->app);
        $response = $controller->post($data);

        $this->assertNotEmpty($response['id']);

        return $response['id'];
    }

    public function testCRUD()
    {
        $data = [
            'status' => true,
            'description' => 'Drupal to Saleforce mapping',
            'source' => [
                'service_instance' => $this->createDrupalInstance(),
                'entity_type' => 'drupal.entity',
                'remote_entity_type' => 'user',
            ],
            'destination' => [
                'service_instance' => $this->createSaleforceInstance(),
                'entity_type' => 'salesforce.entity',
                'remote_entity_type' => 'Contact',
            ],
            'mapping' => [
                ['source' => 'uid', 'destination' => 'id'],
                ['source' => 'mail', 'destination' => 'email']
            ]
        ];

        $controller = new MappingController($this->app);

        // ---------------------
        // Create
        // ---------------------
        $response = $controller->post($data);
        $this->assertArrayHasKey('id', $response);
        $this->assertGreaterThan(0, $response['id']);

        // ---------------------
        // Get
        // ---------------------
        $mapping = $controller->get($response['id']);
        $this->assertGreaterThan(0, $mapping['id']);
        $this->assertTrue($mapping['status']);
        $this->assertEquals($data['description'], $mapping['description']);

        // ---------------------
        // Update
        // ---------------------
        $data['description'] .= ' (updated)';
        $controller->put($mapping['id'], $data);
        $mapping = $controller->get($mapping['id']);
        $this->assertEquals($data['description'], $mapping['description']);

        // ---------------------
        // Delete
        // ---------------------
        $controller->delete($mapping['id']);
        $this->setExpectedException('Luracast\Restler\RestException');
        $controller->get($mapping['id']);
    }

}
