<?php

namespace GoCatalyze\SyncApp\Testing\Controller;

use GoCatalyze\SyncApp\Controller\QueueJobController;
use GoCatalyze\SyncApp\Testing\BaseTestCase;

/**
 * @group QueueJobController
 */
class QueueJobControllerTest extends BaseTestCase
{

    /**
     * @expectedException \Luracast\Restler\RestException
     * @expectedExceptionMessage Invalid sync-mapping ID.
     */
    public function testCreateInvalidSyncMapping()
    {
        $controller = new QueueJobController($this->app);
        $data = $this->dummyEntityQueue('create', 'drupal');

        // no valid sync_mapping => error
        $data['sync_mapping'] = -12345;

        // Action
        $controller->post($data);
    }

    public function testCreate()
    {
        $controller = new QueueJobController($this->app);
        $data = $this->dummyEntityQueue('create', 'drupal');
        $response = $controller->post($data);
        $this->assertEquals('OK', $response['status']);
        $this->assertNotEmpty($response['id']);
    }

}
