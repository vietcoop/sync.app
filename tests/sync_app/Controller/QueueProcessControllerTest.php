<?php

namespace GoCatalyze\SyncApp\Testing\Controller;

use GoCatalyze\SyncApp\Controller\QueueProcessController;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use GoCatalyze\SyncApp\Testing\BaseTestCase;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceService;

/**
 * @group QueueProcessController
 */
class QueueProcessControllerTest extends BaseTestCase
{

    protected $need_db = true;

    public function testProcess()
    {
        // Process queue item, reload item, make sure the status is changed.
        $controller = new QueueProcessController($this->app);

        // Dummy
        /* @var $sync_mapping SyncMappingEntity  */
        $job = $this->dummyQueueItem('drupal', 'salesforce', 'create', $this->dummyDrupalInput());
        $sync_mapping = $job->getArgs()['sync_mapping'];
        $si = $sync_mapping->getSourceServiceInstance();
        $di = $sync_mapping->getDestinationServiceInstance();
        $drupal_service = new DrupalService();
        $drupal_service->setConfiguration($si->getOptions());
        $drupal_entity = new DrupalEntity();
        $drupal_entity->setAttributeValues($job->getArgs()['attributes']);
        $sf_service = new SalesforceService();
        $sf_service->setConfiguration($di->getOptions());
        $sf_entity = new SalesforceEntity();
        $sf_entity['drupal_id__c'] = $job->getArgs()['attributes']['uid'];
        $sf_entity['Mail'] = $job->getArgs()['attributes']['mail'];

        // controller.doProcessQueueItem > call DrupalService::loadFull()
        //  > entityFullLoader > should return source entity ifself
        $drupal_full_loader = $this->getMockBuilder('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader')
            ->disableOriginalConstructor()
            ->getMock();
        $drupal_full_loader->expects($this->once())
            ->method('load')
            ->willReturn($drupal_entity);
        $drupal_service->setEntityFullLoader($drupal_full_loader);

        // Mock › Salesforce client
        $sf_client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceClient');
        $sf_client->expects($this->once())
            ->method('doCreate')
            ->with($sf_entity, 'Contact')
            ->willReturn($sf_entity->getAttributes())
        ;
        $sf_service->setClient($sf_client);

        // Mock › manager
        $manager = $this->getMock('GoCatalyze\SyncCenter\BaseManager');

        // controller.doProcessQueueItem > getService 2 times
        $manager->expects($this->any())
            ->method('getService')
            ->withConsecutive(
                ['drupal', $this->anything()], ['salesforce', $this->anything()]
            )
            ->willReturnOnConsecutiveCalls($drupal_service, $sf_service);

        // controller.doProcessQueueItem > create Drupal entity from array
        $manager->expects($this->once())
            ->method('entityFromArray')
            ->with($job->getArgs()['attributes'], 'drupal.entity', 'user')
            ->willReturn($drupal_entity);

        // controller.doProcessQueueItem > get entity convertor
        $manager->expects($this->once())
            ->method('getEntityConvertor')
            ->willReturn(new EntityConvertor($manager));

        // controller.doProcessQueueItem > create empty salesforce entity
        $manager->expects($this->once())
            ->method('getEntityType')
            ->with('salesforce.entity')
            ->willReturn(new SalesforceEntity());

        $controller->setManager($manager);

        // Action
        $response = $controller->doGet($job);

        // Check result
        $this->assertEquals($job->getId(), $response['id']);
        $this->assertEquals($sf_entity->getAttributes(), $response['response']);
    }

}
