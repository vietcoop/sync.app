<?php

namespace GoCatalyze\SyncApp\Testing\Controller;

use GoCatalyze\SyncApp\Controller\ServiceInstaceController;
use GoCatalyze\SyncApp\Testing\BaseTestCase;

/**
 * @group ServiceInstanceController
 */
class ServiceInstanceControllerTest extends BaseTestCase
{

    public function testCRUD()
    {
        $data = [
            'service_name' => 'drupal',
            'description'  => 'A demo Drupal Instance',
            'options'      => [
                'host'  => 'http://127.0.0.1/drupal',
                'token' => 'SAMPLE TOKEN'
            ],
            'entity_info'  => [
                'node' => ['entity keys' => ['id' => 'nid']],
                'user' => ['entity keys' => ['id' => 'uid']]
            ]
        ];

        $controller = new ServiceInstaceController($this->app);

        // ---------------------
        // Create
        // ---------------------
        $response = $controller->post($data);
        $this->assertArrayHasKey('id', $response);
        $this->assertGreaterThan(0, $response['id']);

        // ---------------------
        // Update
        // ---------------------
        $data['description'] .= ' (edited)';
        $response = $controller->put($response['id'], $data);
        $this->assertGreaterThan(0, $response['id']);

        // ---------------------
        // Get
        // ---------------------
        $response = $controller->get($response['id']);
        $this->assertEquals(['id' => $response['id']] + $data, [
            'id'           => $response['id'],
            'service_name' => $response['service_name'],
            'description'  => $response['description'],
            'options'      => $response['options'],
            'entity_info'  => $response['entity_info']
        ]);

        // Delete
        $response = $controller->delete($response['id']);
        $this->assertEmpty($response);
    }

}
