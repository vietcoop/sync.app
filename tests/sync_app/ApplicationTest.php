<?php

namespace GoCatalyze\SyncApp\Testing;

class ApplicationTest extends BaseTestCase
{

    public function testApplication()
    {


        $this->assertInstanceOf('GoCatalyze\SyncApp\Application', $this->app);
        $this->assertEquals('testing', $this->app->getMode());

        // ---------------------
        // Application variables
        // ---------------------
        $this->app->variableSet('foo', 'FOO');
        $this->assertEquals('FOO', $this->app->variableGet('foo'));
        $this->app->variableSet('bar', 'BAR');
        $this->assertEquals('BAR', $this->app->variableGet('bar'));

        // ---------------------
        // Services
        // ---------------------
        $this->assertInstanceOf('Luracast\Restler\Restler', $this->app->getRoute());
        $this->assertInstanceOf('Doctrine\ORM\EntityManagerInterface', $this->app->getEntitiyManager());
        $this->assertInstanceOf('Symfony\Component\Validator\Validator\ValidatorInterface', $this->app->getValidator());
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ManagerInterface', $this->app->getSyncManager());
    }

}
