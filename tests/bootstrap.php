<?php

use GoCatalyze\SyncApp\Application;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalExtension;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceExtension;
use GoCatalyze\SyncCenter\Testing\Fixtures\DemoExtension;

// ---------------------
// Auto class loading
// ---------------------
$locations = [
    __DIR__ . "/../vendor/autoload.php",
    __DIR__ . "/../../../autoload.php"
];

foreach ($locations as $location) {
    if (is_file($location)) {
        $loader = require $location;
        $loader->addPsr4('GoCatalyze\\SyncApp\\Testing\\', __DIR__ . '/sync_app');
        $loader->addPsr4('GoCatalyze\\SyncApp\\Testing\\Fixtures\\', __DIR__ . '/fixtures');

        $rclass = new ReflectionClass('GoCatalyze\SyncCenter\BaseManager');
        $dir = dirname(dirname($rclass->getFileName())) . '/tests/fixtures';
        $loader->addPsr4('GoCatalyze\\SyncCenter\\Testing\\Fixtures\\', $dir);
        break;
    }
}

// ---------------------
// Important objects for test cases
// ---------------------
final class SyncCenterTestManager
{

    /**
     * @return Application
     */
    public static function getApplication()
    {
        static $app;

        if (is_null($app)) {
            $app = new Application(dirname(__DIR__), '/config/config_testing.php');
            $app->addEntityDir(__DIR__ . '/fixtures/Entity');

            $manager = $app->getSyncManager();
            $manager->registerExtension(new DrupalExtension());
            $manager->registerExtension(new SalesforceExtension());
            $manager->registerExtension(new DemoExtension());
        }

        return $app;
    }

}
