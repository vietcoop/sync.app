<?php

namespace GoCatalyze\SyncApp\Controller;

/**
 * Queue process controller.
 */
class InfoController extends ApplicationAwareController
{

    public function index()
    {
        $manager = $this->app->getSyncManager();

        $info = [
            'extensions'   => $manager->getExtensionNames(),
            'entity_types' => $manager->getEntityTypeNames(),
        ];

        foreach ($manager->getServiceNames() as $service_name) {
            $info['services'][$service_name] = [
                'name'          => $service_name,
                'config_schema' => $manager->getService($service_name, [])->getConfigurationSchema()
            ];
        }

        return $info;
    }

}
