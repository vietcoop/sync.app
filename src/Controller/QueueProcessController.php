<?php

namespace GoCatalyze\SyncApp\Controller;

use GoCatalyze\SyncApp\Controller\ApplicationAwareController;
use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\JobProcessor;
use Luracast\Restler\RestException;

/**
 * Queue process controller.
 */
class QueueProcessController extends ApplicationAwareController
{

    /**
     * @codeCoverageIgnore
     */
    public function index()
    {
        return $this->get();
    }

    /**
     * Process an oldest item.
     *
     * @url GET /{id}
     * @codeCoverageIgnore
     */
    public function get($id = null)
    {
        /* @var $job QueueJobEntity */
        if (!$job = $this->app->getQueueJob($id)) {
            throw new RestException(404, 'There is no item in queue.');
        }

        switch ($job->getState()) {
            // if process a already failed/finished job, clone the failed job, then
            // process the new one.
            case QueueJobEntity::STATE_FINISHED:
            case QueueJobEntity::STATE_FAILED:
                // @todo: Check maxRetries
                $new_job = new QueueJobEntity($job->getCommand(), $job->getArgs());
                $new_job->setOriginalJob($job);
                $new_job->setMd5(md5($job->getMd5() . ':cloneof:' . $job->getId() . ':' . time()));
                $em = $this->app->getEntitiyManager();
                $em->persist($new_job);
                $em->flush();
                return $this->get($new_job->getId());

            case QueueJobEntity::STATE_RUNNING:
                throw new RestException(400, 'The job is being processed.');

            default:
                try {
                    return (new JobProcessor($this->app, $job))->process()->toArray();
                }
                catch (\Exception $e) {
                    throw new RestException(400, 'Failed to process job. Check log for more details.');
                }
                break;
        }
    }

}
