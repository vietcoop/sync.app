<?php

namespace GoCatalyze\SyncApp\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GoCatalyze\SyncApp\Application;
use GoCatalyze\SyncCenter\ManagerInterface;
use stdClass;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class ApplicationAwareController
{

    /** @var Application */
    protected $app;

    /** @var ManagerInterface */
    protected $manager;

    /**
     * Constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        @header('Allow: POST, GET, OPTIONS, PUT, PATCH, DELETE');
        @header('Access-Control-Allow-Origin:  *');
        @header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, PATCH, DELETE');
        @header('Access-Control-Max-Age: 1000');
        @header('Access-Control-Allow-Headers: *');
    }

    public function options()
    {
        return [];
    }

    /**
     * Get entity manager.
     *
     * @return EntityManagerInterface
     */
    protected function getEntityManager()
    {
        return $this->app->getEntitiyManager();
    }

    /**
     * Entity repository.
     *
     * @return EntityRepository
     */
    protected function getEntityRepository()
    {
        return $this->app
                ->getEntitiyManager()
                ->getRepository($this->entity_class)
        ;
    }

    /**
     * Just because Validator::getMetadataFor() return missing type-hint.
     *
     * @param stdClass $object
     * @return ClassMetadata
     */
    protected function getValidatorMetadata($object)
    {
        return $this->app->getValidator()->getMetadataFor($object);
    }

    /**
     * @access private
     */
    public function getManager()
    {
        if (null === $this->manager) {
            $this->manager = $this->app->getSyncManager();
        }
        return $this->manager;
    }

    /**
     * @access private
     */
    public function setManager(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }

}
