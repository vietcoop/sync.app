<?php

namespace GoCatalyze\SyncApp\Controller;

use GoCatalyze\SyncApp\Entity\RemoteEntityInfoEntity;
use GoCatalyze\SyncApp\Entity\ServiceInstanceEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use InvalidArgumentException;
use Luracast\Restler\RestException;
use Symfony\Component\Validator\Constraints\Choice;

class ServiceInstaceController extends ApplicationAwareController
{

    protected $entity_class = 'GoCatalyze\\SyncApp\\Entity\\ServiceInstanceEntity';

    /**
     * Get all service instances.
     *
     * @return ServiceInstanceEntity[]
     */
    public function index()
    {
        $return = [];

        foreach ($this->getEntityRepository()->findAll() as $instance) {
            /* @var $instance ServiceInstanceEntity */
            $return[] = [
                'id'           => $instance->getId(),
                'service_name' => $instance->getServiceName(),
                'description'  => $instance->getDescription(),
                'options'      => $instance->getOptions(false),
            ];
        }

        return $return;
    }

    /**
     * Get a service instance.
     *
     * @param int|string $id
     * @return ServiceInstanceEntity
     */
    public function get($id)
    {
        if ($id === 'all') {
            return $this->index();
        }

        /* @var $instance ServiceInstanceEntity */
        if (!$instance = $this->getEntityRepository()->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        $return = $instance->toArray();

        if (empty($_GET['export'])) {
            foreach ($this->getEntityManager()->getRepository('GoCatalyze\SyncApp\Entity\RemoteEntityInfoEntity')->findBy(['service_instance_id' => $id]) as $info) {
                /* @var $info RemoteEntityInfoEntity */
                $return['entity_info'][$info->getEntityType()] = $info->getDetails()['label'];
            }

            $return['config_schema'] = $this->getManager()
                ->getService($instance->getServiceName(), [])
                ->getConfigurationSchema();
        }

        return $return;
    }

    /**
     * @url GET /{id}/info/{entity_type}/{detail_key}
     *
     * @param int $id
     * @param string $entity_type
     * @param string|null $detail_key
     */
    public function entityInfo($id, $entity_type, $detail_key)
    {
        $repos = $this->getEntityManager()
            ->getRepository('GoCatalyze\SyncApp\Entity\RemoteEntityInfoEntity');
        if (!$info = $repos->findOneBy(['entity_type' => $entity_type, 'service_instance_id' => (int) $id])) {
            throw new \Luracast\Restler\RestException(404, 'Remote entity not found');
        }
        /* @var $info \GoCatalyze\SyncApp\Entity\RemoteEntityInfoEntity */
        return 'all' === $detail_key ? $info->getDetails() : $info->getDetails()[$detail_key];
    }

    public function post(array $request_data = [])
    {
        $instance = new ServiceInstanceEntity();

        foreach ($request_data as $k => $v) {
            try {
                $instance->setPropertyValue($k, $v);
            }
            catch (InvalidArgumentException $e) {
                throw new RestException(400, $e->getMessage());
            }
        }

        $this->getValidatorMetadata($instance)
            ->addPropertyConstraint('service_name', new Choice([
                'message' => 'Service is not enabled on sync center.',
                'choices' => $this->app->getSyncManager()->getServiceNames()
        ]));

        $error = $this->app->getValidator()->validate($instance);
        if (0 === $error->count()) {
            // Fetch entity info if create with options
            if (($options = $instance->getOptions()) && (1 < count($options)) && !$instance->getEntityInfo()) {
                $service = $this->app->getSyncManager()->getService($instance->getServiceName(), $instance->getOptions());
                if ($entity_info = $service->getRemoteEntityInfo()) {
                    $instance->setEntityInfo($entity_info);
                }
            }

            $em = $this->getEntityManager();
            $em->persist($instance);
            $em->flush();
            return ['id' => $instance->getId()];
        }

        throw new RestException(400, (string) $error);
    }

    public function put($id, array $request_data = [])
    {
        if (!$instance = $this->getEntityRepository()->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        foreach ($request_data as $k => $v) {
            try {
                /* @var $instance ServiceInstanceEntity */
                $instance->setPropertyValue($k, $v);
            }
            catch (InvalidArgumentException $e) {
                throw new RestException(400, $e->getMessage());
            }
        }

        $this->getValidatorMetadata($instance)
            ->addPropertyConstraint('service_name', new Choice([
                'message' => 'Service is not enabled on sync center.',
                'choices' => $this->app->getSyncManager()->getServiceNames()
        ]));

        $error = $this->app->getValidator()->validate($instance);

        if (0 === $error->count()) {
            // Update entity info
            $service = $this->app
                ->getSyncManager()
                ->getService($instance->getServiceName(), $instance->getOptions());

            $em = $this->getEntityManager();
            $em->persist($instance);
            $em->flush();
            $em->clear();

            if ($instance->getId() && ($reinfo = $service->getRemoteEntityInfo(null, true))) {
                $q = $em->getRepository('GoCatalyze\SyncApp\Entity\RemoteEntityInfoEntity')->createQueryBuilder('EntityInfo');
                $q->delete();
                $q->where($q->expr()->eq('EntityInfo.service_instance_id', ':sid'));
                $q->setParameter(':sid', $instance->getId());
                $q->getQuery()->execute();

                foreach ($reinfo as $type => $info) {
                    $em->persist(
                        (new RemoteEntityInfoEntity())
                            ->setDetails($info)
                            ->setEntityType($type)
                            ->setServiceInstanceId($instance->getId())
                    );
                }

                $em->flush();
            }

            return ['status' => 'OK', 'id' => $instance->getId()];
        }

        throw new RestException(400, (string) $error);
    }

    public function delete($id)
    {
        $em = $this->getEntityManager();
        $repos = $this->getEntityRepository();

        /* @var $instance ServiceInstanceEntity */
        if (!$instance = $repos->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        // Delete all related mappings
        $q = $em->getRepository('GoCatalyze\SyncApp\Entity\SyncMappingEntity')->createQueryBuilder('Mapping');
        $q->select('Mapping');
        $q->where('Mapping.source_service_instance = :instance OR Mapping.destination_service_instance = :instance');
        $q->setParameter(':instance', $instance);
        $mapping = $q->getQuery()->execute();

        $mctrl = new MappingController($this->app);
        foreach ($q->getQuery()->execute() as $mapping) {
            /* @var $mapping SyncMappingEntity */
            $mctrl->delete($mapping->getId());
        }

        // Flush
        $em->remove($instance);
        $em->flush();

        return [];
    }

}
