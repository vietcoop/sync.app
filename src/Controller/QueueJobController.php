<?php

namespace GoCatalyze\SyncApp\Controller;

use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use GoCatalyze\SyncApp\JobProcessor;
use InvalidArgumentException;
use Luracast\Restler\RestException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class QueueJobController extends ApplicationAwareController
{

    /**
     * Get a job in queue.
     *
     * @url GET /{id}
     * @param int|null $id
     */
    public function get($id = null)
    {
        if ('all' === $id) {
            $limit = $tmp = filter_input(INPUT_GET, 'limit') ? $tmp : 50;
            $offset = $tmp = filter_input(INPUT_GET, 'offset') ? $tmp : 0;
            return $this->app->getQueueJobs($limit, $offset);
        }

        if ($job = $this->app->getQueueJob($id, 'GoCatalyze\SyncApp\Entity\QueueJobEntity')) {
            $return = $job->toArray();

            if (!empty($return['output']) && strpos($return['output'], '{') === 0) {
                if ($_output = json_decode($return['output'])) {
                    $return['output'] = $_output;
                }
            }

            return $return;
        }

        throw new RestException(404, 'Queue job not found.');
    }

    /**
     * Import an entity, create queue command.
     *
     * @param array $data Information about entity — an assosicative array
     *
     *    [
     *      'action' => 'create',
     *      'sync_mapping' => $this->getSyncMapping(),
     *      'attributes' => [
     *          'entity_type' => 'node',
     *          'type' => 'article',
     *          'nid' => 1,
     *          'status' => true,
     *          'created' => strtotime('- 2 days'),
     *          'updated' => strtotime('- 1 day'),
     *          'title' => 'Hello article!',
     *          'body' => ['und' => [0 => ['value' => 'This is body.']]],
     *        ],
     *      ]
     *
     */
    public function post(array $data = [])
    {
        $error = $this->app->getValidator()->validateValue($data, new Collection([
            'action'       => new Choice(['choices' => ['create', 'update', 'merge', 'delete']]),
            'sync_mapping' => new Callback(['callback' => [$this, 'validateSyncMapping']]),
            'attributes'   => new Assert\NotBlank()
        ]));

        if (0 !== $error->count()) {
            throw new RestException(400, 'Invalid input: ' . $error->offsetGet(0)->getMessage());
        }

        // create and queue job
        $mapping_id = is_object($data['sync_mapping']) ? $data['sync_mapping']->getId() : (int) $data['sync_mapping'];
        $job = new QueueJobEntity("syncEntity:Mapping:{$mapping_id}", $data);

        $em = $this->app->getEntitiyManager();
        $em->persist($job);
        $em->flush();

        // give response
        return !$job->getId() ? ['status' => 'FAILED'] : ['status' => 'OK', 'id' => $job->getId()];
    }

    /**
     * Patch a job queue.
     *
     * @param int $id
     * @param array $cmds
     */
    public function patch($id, array $cmds = [])
    {
        if (!$job = $this->app->getQueueJob($id)) {
            throw new RestException(404);
        }

        try {
            if ($patched = $this->doPatch($job, $cmds)) {
                $em = $this->app->getEntitiyManager();
                $em->persist($job);
                $em->flush();
            }
        }
        catch (\Exception $e) {
            return ['status' => 'FAILED', 'error' => true, 'message' => $e->getMessage()];
        }

        return ['status' => 'OK'];
    }

    private function doPatch(QueueJobEntity $job, array $cmds)
    {
        $patched = false;

        foreach ($cmds as $cmd) {
            if ('replace' !== $cmd['op']) {
                throw new InvalidArgumentException('Unsupported operation.');
            }

            $patched = true;
            $job->setPropertyValue($cmd['path'], $cmd['value']);
        }

        return $patched;
    }

    public function validateSyncMapping($id, ExecutionContextInterface $context)
    {
        if (!$id instanceof SyncMappingEntity) {
            $mapping = $this->getEntityManager()
                ->getRepository('GoCatalyze\SyncApp\Entity\SyncMappingEntity')
                ->find($id);

            if (!$mapping) {
                $context->addViolation('Invalid sync-mapping ID.');
            }
        }
    }

    /**
     * Process a queue job.
     *
     * @url GET /process/{id}
     * @param int $id
     */
    public function process($id = null)
    {
        if (!$job = $this->app->getQueueJob($id)) {
            throw new RestException(404);
        }

        try {
            return (new JobProcessor($this->app, $job))->process()->toArray();
        }
        catch (\Exception $e) {
            throw new RestException(400, 'Failed to process job. Check log for more details.');
        }
    }

    /**
     * Delete a job from queue.
     *
     * @url DELETE /{id}
     * @param int $id
     * @return array
     * @throws RestException
     */
    public function deleteJob($id)
    {
        if (!$job = $this->app->getQueueJob($id)) {
            throw new RestException(404, 'Job not found.');
        }

        $em = $this->app->getEntitiyManager();
        $em->remove($job);
        $em->flush();

        return ['status' => 'OK'];
    }

    /**
     * Clean up completed/failed jobs.
     *
     * @url GET /cleanup
     */
    public function cleanUpJobs()
    {
        $repos = $this->app->getEntitiyManager();
        $q = $repos->createQueryBuilder();
        $q->delete();
        $q->from('GoCatalyze\SyncApp\Entity\QueueJobEntity', 'Job');
        $q->andWhere($q->expr()->in('Job.state', [QueueJobEntity::STATE_FINISHED, QueueJobEntity::STATE_CANCELED, QueueJobEntity::STATE_FAILED]));
        $rows = $q->getQuery()->execute();
        return ['status' => 'OK', 'rows' => $rows];
    }

}
