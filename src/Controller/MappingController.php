<?php

namespace GoCatalyze\SyncApp\Controller;

use DateTime;
use GoCatalyze\SyncApp\Controller\ApplicationAwareController;
use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use InvalidArgumentException;
use Luracast\Restler\RestException;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;

class MappingController extends ApplicationAwareController
{

    /**
     * @var string
     */
    protected $entity_class = 'GoCatalyze\\SyncApp\\Entity\\SyncMappingEntity';

    /**
     * List all sync-mappings.
     *
     * return array
     */
    public function index()
    {
        $return = [];

        foreach ($this->getEntityRepository()->findAll() as $mapping) {
            /* @var $mapping SyncMappingEntity */
            $return[] = [
                'id'          => $mapping->getId(),
                'status'      => $mapping->getStatus(),
                'updated_at'  => $mapping->getUpdatedAt(),
                'run_at'      => $mapping->getUpdatedAt(),
                'description' => $mapping->getDescription(),
                'source'      => [
                    'service'            => $mapping->getSourceServiceInstance()->getServiceName(),
                    'instance'           => [
                        'id'          => $mapping->getSourceServiceInstance()->getId(),
                        'description' => $mapping->getSourceServiceInstance()->getDescription(),
                    ],
                    'entity_type'        => $mapping->getSourceEntityType(),
                    'remote_entity_type' => $mapping->getSourceRemoteEntityType(),
                ],
                'destination' => [
                    'service'            => $mapping->getDestinationServiceInstance()->getServiceName(),
                    'instance'           => [
                        'id'          => $mapping->getDestinationServiceInstance()->getId(),
                        'description' => $mapping->getDestinationServiceInstance()->getDescription(),
                    ],
                    'entity_type'        => $mapping->getDestinationEntityType(),
                    'remote_entity_type' => $mapping->getDestinationRemoteEntityType()
                ],
                'mapping'     => $mapping->getMappingInfo(),
                'queued_jobs' => $this->app->countQueueJobs("syncEntity:Mapping:{$mapping->getId()}"),
            ];
        }

        return $return;
    }

    /**
     * Get a sync-mapping by ID.
     *
     * @url   GET /{id}
     * @param int $id
     */
    public function get($id = null)
    {
        /* @var $sync_mapping SyncMappingEntity */
        if (!$sync_mapping = $this->doGet($id)) {
            throw new RestException(404);
        }
        return $sync_mapping->toArray();
    }

    /**
     * Get mapping by ID.
     *
     * @param int $id
     * @return SyncMappingEntity
     */
    private function doGet($id = null)
    {
        $repos = $this->getEntityRepository();

        if (null !== $id) {
            if ($sync_mapping = $repos->findOneBy(['id' => $id])) {
                return $sync_mapping;
            }
        }
        elseif ($sync_mapping = $this->getEntityRepository()->findOneBy([], ['run_at' => 'ASC'])) {
            return $sync_mapping;
        }

        return null;
    }

    /**
     * Create new sync-mapping.
     *
     * @param array $request_data
     */
    public function post(array $request_data = [])
    {
        $mapping = new SyncMappingEntity();

        // Validate input
        $input_error = $this->app->getValidator()->validateValue($request_data, new Collection([
            'status'      => new Choice(['choices' => [true, false]]),
            'priority'    => new Choice(['choices' => [QueueJobEntity::PRIORITY_LOW, QueueJobEntity::PRIORITY_DEFAULT, QueueJobEntity::PRIORITY_HIGH]]),
            'description' => new NotBlank(),
            'source'      => new Collection([
                'service_instance'   => new NotBlank(),
                'entity_type'        => new Choice(['choices' => $this->app->getSyncManager()->getEntityTypeNames()]),
                'remote_entity_type' => new NotBlank()
                ]),
            'destination' => new Collection([
                'service_instance'   => new NotBlank(),
                'entity_type'        => new Choice(['choices' => $this->app->getSyncManager()->getEntityTypeNames()]),
                'remote_entity_type' => new NotBlank(),
                'unique_fields'      => new Optional()
                ]),
            'mapping'     => new NotBlank(),
        ]));

        if ($input_error->count()) {
            throw new RestException(400, (string) $input_error);
        }

        /* @var $mapping SyncMappingEntity */
        foreach ($request_data as $k => $v) {
            try {
                // convert ID int to service instance object
                if (in_array($k, ['source', 'destination'])) {
                    if (is_array($v) && isset($v['service_instance']) && is_int($v['service_instance'])) {
                        $v['service_instance'] = $this->getEntityManager()
                            ->getRepository('GoCatalyze\SyncApp\Entity\ServiceInstanceEntity')
                            ->findOneBy(['id' => $v['service_instance']])
                        ;
                    }
                }
                $mapping->setPropertyValue($k, $v);
            }
            catch (InvalidArgumentException $e) {
                throw new RestException(400, $e->getMessage());
            }
        }

        $error = $this->app->getValidator()->validate($mapping);
        if (0 === $error->count()) {
            $mapping->setUpdatedAt(new DateTime('- 1 year'));
            $em = $this->getEntityManager();
            $em->persist($mapping);
            $em->flush();
            return ['id' => $mapping->getId()];
        }

        throw new RestException(400, (string) $error);
    }

    /**
     * Patch mapping object.
     *
     * @param  int    $id
     * @param  array  $cmds
     * @throws RestException
     */
    public function patch($id, array $cmds = [])
    {
        /* @var $mapping SyncMappingEntity */
        if (!$mapping = $this->getEntityRepository()->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        try {
            $persit = false;
            foreach ($cmds as $cmd) {
                // $cmds is associative array, see http://j.mp/1myy25Q
                //    'op'    => string test/remove/add/replace/move/copy
                //    'path'  => string Path to property,
                //    'value' => mixed
                switch ($cmd['op']) {
                    case 'replace':
                        $persit = true;
                        $mapping->setPropertyValue($cmd['path'], $cmd['value']);
                        break;
                    default:
                        throw new \InvalidArgumentException('Unsupported operation.');
                }
            }

            if ($persit) {
                $em = $this->app->getEntitiyManager();
                $em->persist($mapping);
                $em->flush();
            }
        }
        catch (\Exception $e) {
            return [
                'status'  => 'FAILED',
                'error'   => true,
                'message' => $e->getMessage()
            ];
        }

        return ['status' => 'OK'];
    }

    /**
     * Update a sync mapping.
     *
     * @param int $id
     * @param array $request_data
     */
    public function put($id, array $request_data = [])
    {
        if (!$mapping = $this->getEntityRepository()->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        /* @var $mapping SyncMappingEntity */
        foreach ($request_data as $k => $v) {
            try {
                switch ($k) {
                    case 'source':
                    case 'destination':
                        if (is_integer($v['service_instance'])) {
                            $v['service_instance'] = $this->getEntityManager()
                                ->getRepository('GoCatalyze\SyncApp\Entity\ServiceInstanceEntity')
                                ->findOneBy(['id' => $v['service_instance']])
                            ;
                        }
                        break;
                }
                $mapping->setPropertyValue($k, $v);
            }
            catch (InvalidArgumentException $e) {
                throw new RestException(400, $e->getMessage());
            }
        }

        $error = $this->app->getValidator()->validate($mapping);
        if (0 === $error->count()) {
            // force update update_at
            $mapping->setUpdatedAt(new DateTime());

            $em = $this->getEntityManager();
            $em->persist($mapping);
            $em->flush();
            $em->clear();

            // Update priority of queued jobs
            $query = $em->createQuery("UPDATE GoCatalyze\SyncApp\Entity\QueueJobEntity Job"
                . " SET Job.priority = :priority"
                . " WHERE Job.command = :command");
            $query->setParameter(':command', "syncEntity:Mapping:" . $mapping->getId());
            $query->setParameter(':priority', $mapping->getPriority());
            $query->execute();

            // Cancel queue-jobs if user disables mapping, vice-v.
            $state_old = $mapping->getStatus() ? QueueJobEntity::STATE_CANCELED : QueueJobEntity::STATE_PENDING;
            $state_new = $mapping->getStatus() ? QueueJobEntity::STATE_PENDING : QueueJobEntity::STATE_CANCELED;
            $state_query = $em->createQuery("UPDATE GoCatalyze\SyncApp\Entity\QueueJobEntity Job"
                . " SET Job.state = :new_state"
                . " WHERE Job.command = :command AND Job.state = :old_state");
            $state_query->setParameter(':command', "syncEntity:Mapping:" . $mapping->getId());
            $state_query->setParameter(':old_state', $state_old);
            $state_query->setParameter(':new_state', $state_new);
            $state_query->execute();

            return ['id' => $mapping->getId()];
        }

        throw new RestException(400, (string) $error);
    }

    /**
     * Delete a sync-mapping by ID.
     *
     * @param int $id
     */
    public function delete($id)
    {
        $em = $this->getEntityManager();
        $repos = $this->getEntityRepository();

        /* @var $mapping SyncMappingEntity */
        if (!$mapping = $repos->findOneBy(['id' => $id])) {
            throw new RestException(404);
        }

        // delete queue jobs
        $q = $em->createQueryBuilder();
        $q->delete('GoCatalyze\SyncApp\Entity\QueueJobEntity', 'Job');
        $q->andWhere($q->expr()->eq('Job.command', ':command'));
        $q->setParameter(':command', 'syncEntity:Mapping:' . $mapping->getId());
        $q->getQuery()->execute();

        $em->remove($mapping);
        $em->flush();

        return ['status' => 'OK'];
    }

    /**
     * Endpoint to fetch a mapping.
     *
     * @url GET /fetch
     */
    public function fetchNull()
    {
        return $this->fetch();
    }

    /**
     * Fetch source service for new entities, create queue-jobs where workers
     * can do later.
     *
     * @url   GET /{id}/fetch
     * @param int $id Mapping ID.
     */
    public function fetch($id = null)
    {
        $em = $this->app->getEntitiyManager();
        $repos = $em->getRepository('GoCatalyze\SyncApp\Entity\QueueJobEntity');
        $start = microtime(true);

        /* @var $mapping SyncMappingEntity */
        if (!$mapping = $this->doGet($id)) {
            throw new RestException(404);
        }

        // start fetching
        $mapping->setProcessing(true);
        $em->persist($mapping);
        $em->flush();

        // with results, create queue jobs
        list($id_key, $entities) = $this->doFetch($mapping);
        foreach ($entities as $entity) {
            /* @var $entity EntityInterface */
            $cmd = "syncEntity:Mapping:{$mapping->getId()}";
            $seed = !empty($id_key) ? "{$cmd}:merge:" . $entity[$id_key] : md5(serialize($entity->getAttributes()));
            $md5 = md5($seed);
            $args = [
                'action'       => 'merge', // Just use merge for now per we do not know when create, when update.
                'sync_mapping' => $mapping->getId(),
                'attributes'   => $entity->getAttributes()
            ];

            // do not create if this is a duplication
            if (!$repos->findOneBy(['md5' => $md5])) {
                $job = new QueueJobEntity($cmd, $args);
                $job->setMd5($md5);
                $em->persist($job);
            }
        }
        $em->flush();

        // update running flags, last run time
        $mapping->setProcessing(false);
        $mapping->setRunAt(new DateTime());
        $em->flush($mapping);

        return [
            'status'        => 'OK',
            'id'            => $mapping->getId(),
            'runtime'       => microtime(true) - $start,
            'entity_number' => count($entities),
        ];
    }

    /**
     * Fetch source service for new entities.
     *
     * @param SyncMappingEntity $mapping
     * @return array
     */
    private function doFetch(SyncMappingEntity $mapping)
    {
        $manager = $this->app->getSyncManager();

        $service_instance = $mapping->getSourceServiceInstance();
        $type = $mapping->getSourceRemoteEntityType();

        // Get service object
        $service_name = $service_instance->getServiceName();
        $service_options = $service_instance->getOptions();
        $service = $manager->getService($service_name, $service_options);

        // Get query object
        $query = $manager->getEntityQuery($mapping->getSourceEntityType());
        $query->queryLatestEntities($mapping->getRunAt(), $mapping->getSourceRemoteEntityType());

        $entities = [];
        foreach ($service->query($query, $type) as $entity_attributes) {
            $entity = $manager->getEntityType($mapping->getSourceEntityType());
            $entity->setAttributeValues($entity_attributes);
            $entities[] = $entity;
        }

        return [$service->getRemoteEntityIdKey($type), $entities];
    }

    /**
     * Get queue jobs.
     *
     * @url   GET /{id}/queue
     */
    public function getQueueJobs($id)
    {
        if (!$mapping = $this->doGet($id)) {
            throw new RestException(404);
        }

        $limit = ($tmp = filter_input(INPUT_GET, 'limit')) ? $tmp : 50;
        $offset = ($tmp = filter_input(INPUT_GET, 'offset')) ? $tmp : 0;
        $cmd = "syncEntity:Mapping:{$mapping->getId()}";

        return $this->app->getQueueJobs($limit, $offset, 'GoCatalyze\SyncApp\Entity\QueueJobEntity', $cmd, true);
    }

    /**
     * Get job in mapping.
     *
     * @url   GET /{id}/queue/{job_id}
     * @param int $id
     * @param int|null $job_id
     */
    public function getQueueJob($id, $job_id = null)
    {
        if (!$mapping = $this->doGet($id)) {
            throw new RestException(404);
        }

        return (new QueueJobController($this->app))->get($job_id);
    }

    /**
     * Delete a job from queue.
     *
     * @url DELETE /{id}/queue/{job_id}
     * @param int $id
     * @param int $job_id
     */
    public function deleteQueueJob($id, $job_id)
    {
        return (new QueueJobController($this->app))->deleteJob($job_id);
    }

}
