<?php

namespace GoCatalyze\SyncApp;

use DateTime;
use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use GoCatalyze\SyncApp\Entity\SyncMappingEntity;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\ManagerInterface;
use GoCatalyze\SyncCenter\ServiceInterface;
use RuntimeException;

class JobProcessor
{

    /** @var Application */
    private $app;

    /** @var ManagerInterface */
    private $manager;

    /** @var QueueJobEntity */
    private $job;

    /** @var SyncMappingEntity */
    private $mapping;

    public function __construct(Application $app, QueueJobEntity $job)
    {
        $this->app = $app;
        $this->manager = $this->app->getSyncManager();
        $this->job = $job;

        if ($this->job->getArgs()['sync_mapping'] instanceof SyncMappingEntity) {
            $this->mapping = $this->job->getArgs()['sync_mapping'];
        }
        else {
            $this->mapping = $this->app
                ->getEntitiyManager()
                ->getRepository('GoCatalyze\SyncApp\Entity\SyncMappingEntity')
                ->find($this->job->getArgs()['sync_mapping']);
        }
    }

    /**
     * Get information needed to push to destination service.
     *
     * @return array
     */
    private function getDestinationInfo()
    {
        // Get needed objects from sync-mapping
        $s_type = $this->mapping->getSourceEntityType();
        $s_rtype = $this->mapping->getSourceRemoteEntityType();
        $s_instance = $this->mapping->getSourceServiceInstance();
        $s_service = $this->manager->getService($s_instance->getServiceName(), $s_instance->getOptions());
        $d_type = $this->mapping->getDestinationEntityType();
        $d_instance = $this->mapping->getDestinationServiceInstance();
        $d_service = $this->manager->getService($d_instance->getServiceName(), $d_instance->getOptions());
        $d_query = $this->manager->getEntityQuery($d_type);
        $d_uniques = $this->mapping->getDestinationUniqueFields();
        $field_mapping = EntityMapping::fromArray($this->mapping->getMappingInfo());

        // Create source entity from queue command
        $s_entity = $s_service->loadFull($this->manager->entityFromArray($this->job->getArgs()['attributes'], $s_type, $s_rtype), $s_rtype, $field_mapping);

        // convert source to dest.entity
        $d_rtype = $this->mapping->getDestinationRemoteEntityType();
        $d_entity = $this->manager->getEntityConvertor()
            ->setSourceService($s_service)
            ->setDestinationService($d_service)
            ->setDestinationEntityQueryBuilder($d_query)
            ->setDestinationType($d_type)
            ->setDestinationRemoteType($d_rtype)
            ->setDestinationUniqueFields($d_uniques)
            ->convert($s_entity, $d_type, $field_mapping);

        return [$d_service, $d_entity, $d_rtype, $d_uniques, $field_mapping];
    }

    /**
     * @return QueueJobEntity
     */
    public function process()
    {
        $em = $this->app->getEntitiyManager();
        $time_start = microtime(true);

        try {
            $this->mapping->setRunAt(new DateTime());
            $this->job->setState(QueueJobEntity::STATE_RUNNING);

            $em->persist($this->job);
            $em->flush();

            ob_start();
            $output = $this->doProcess();
            $error_output = ob_get_contents();
            ob_end_clean();

            if ($output) {
                if (!is_string($output)) {
                    if (is_array($output)) {
                        $output = json_encode($output);
                    }
                    elseif ($output instanceof \GoCatalyze\SyncCenter\Entity\EntityInterface) {
                        $output = json_encode($output->getAttributes());
                    }
                    else {
                        $output = serialize($output);
                    }
                }
                $this->job->setOutput($output);
            }

            if (!empty($error_output)) {
                $this->job->setErrorOutput($error_output);
            }

            // update job status to successed
            $this->job->setState(QueueJobEntity::STATE_FINISHED);
        }
        catch (\Exception $e) {
            $this->job->setState(QueueJobEntity::STATE_FAILED);
            $this->job->setStackTrace($e->getTraceAsString());
            $this->job->setErrorOutput($e->getMessage());
        }

        $this->job->setRuntime(microtime(true) - $time_start);
        $this->job->setMemoryUsage(memory_get_usage());
        $this->job->setMemoryUsageReal(memory_get_usage(true));

        $em->persist($this->job);
        $em->flush();

        return $this->job;
    }

    private function doProcess()
    {
        /* @var $d_service ServiceInterface */
        /* @var $d_entity EntityInterface */
        /* @var $d_rty string */
        /* @var $d_uniques string[] */
        /* @var $field_mapping EntityMappingInterface */
        list($d_service, $d_entity, $d_rtype, $d_uniques, $field_mapping) = $this->getDestinationInfo();

        // Get & perform action
        switch ($this->job->getArgs()['action']) {
            case 'create':
            case 'insert':
                return $d_service->create($d_entity, $d_rtype);

            case 'update':
                $id = ''; // @TODO!
                return $d_service->update($d_entity, $id, $d_rtype);

            case 'save':
            case 'merge':
                return $d_service->merge($d_entity, $d_rtype, $field_mapping, $d_uniques);

            case 'delete':
            case 'remove':
                throw new RuntimeException('Action to be provided: ' . $this->job->data['action']);

            default:
                throw new RuntimeException('Unsupported action.');
        }
    }

}
