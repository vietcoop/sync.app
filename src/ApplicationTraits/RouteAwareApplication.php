<?php

namespace GoCatalyze\SyncApp\ApplicationTraits;

use Luracast\Restler\Restler;
use Luracast\Restler\Scope;

trait RouteAwareApplication
{

    /**
     * Route
     *
     * @var Restler
     */
    protected $route;

    /**
     * Default resources.
     *
     * @var array
     */
    protected $route_resources = [
        'info'     => 'GoCatalyze\SyncApp\Controller\InfoController',
        'instance' => 'GoCatalyze\\SyncApp\\Controller\\ServiceInstaceController',
        'mapping'  => 'GoCatalyze\\SyncApp\\Controller\\MappingController',
        'queue'    => 'GoCatalyze\SyncApp\Controller\QueueJobController',
        // @TODO Merge this to QueueJob controller — GET /queue/process/:id
        'process'  => 'GoCatalyze\SyncApp\Controller\QueueProcessController',
    ];

    /**
     * Getter for route property.
     *
     * @return Restler
     */
    public function getRoute()
    {
        if (is_null($this->route)) {
            $route = new Restler('production' === $this->mode, $this->debug);

            // Adding resources
            $route->addAPIClass('Resources');
            $app = $this;
            foreach ($this->route_resources as $start => $class_name) {
                $route->addAPIClass($class_name, $start);
                Scope::register($class_name, function() use ($app, $class_name) {
                    return new $class_name($app);
                });
            }

            $this->setRoute($route);
        }

        return $this->route;
    }

    /**
     * Setter for route property.
     *
     * @param Restler $route
     */
    public function setRoute(Restler $route)
    {
        $this->route = $route;
    }

}
