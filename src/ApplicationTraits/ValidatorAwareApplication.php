<?php

namespace GoCatalyze\SyncApp\ApplicationTraits;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait ValidatorAwareApplication
{

    /**
     * Validator.
     *
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Setter for validator property.
     *
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Getter for validator property.
     *
     * @return ValidatorInterface
     */
    public function getValidator()
    {
        if (null === $this->validator) {
            $this->setValidator($this->getDefaultValidator());
        }

        return $this->validator;
    }

    /**
     * @return ValidatorInterface
     */
    protected function getDefaultValidator()
    {
        return Validation::createValidatorBuilder()
                ->addMethodMapping('loadValidatorMetadata')
                ->getValidator()
        ;
    }

}
