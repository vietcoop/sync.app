<?php

namespace GoCatalyze\SyncApp\ApplicationTraits;

trait BaseApplication
{

    /**
     * Application's running mode.
     *
     * @var string
     */
    protected $mode = 'production';

    /**
     * Debugging.
     *
     * @var boolean
     */
    protected $debug = false;

    /**
     * Application variable.
     *
     * @var array
     */
    protected $variables = [];

    /**
     * Root directory of application.
     *
     * @var string
     */
    protected $app_root = './';

    /**
     * Constructor
     *
     * @param string $config_file
     */
    public function __construct($app_root, $config_file = '/config/config.php')
    {
        // App Root
        $this->app_root = rtrim($app_root, '/');

        // App configuration
        $config = require $this->app_root . '/' . ltrim($config_file, '/');

        if (!is_array($config)) {
            throw new \RuntimeException('Configuration must be an array.');
        }

        if (isset($config['mode'])) {
            $this->mode = $config['mode'];
            unset($config['mode']);
        }

        if (isset($config['debug'])) {
            $this->debug = $config['debug'];
            unset($config['debug']);
        }

        // inject variables
        $this->variablesSet($config);

        // Auth
        $this->basicAuth();
    }

    /**
     * Get app's running mode.
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Get root directory of application.
     *
     * @return string
     */
    public function getAppRootDir()
    {
        return $this->app_root;
    }

    /**
     * Set app's mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * Variable getter.
     *
     * @param string $name
     */
    public function variableGet($name)
    {
        return isset($this->variables[$name]) ? $this->variables[$name] : null;
    }

    /**
     * Variable setter.
     *
     * @param name $name
     * @param mixed $value
     */
    public function variableSet($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /**
     *
     * @param array $variables
     */
    public function variablesSet(array $variables)
    {
        $this->variables = array_merge($variables, $this->variables);
    }

    /**
     * Get all appliation's variables.
     *
     * @return array
     */
    public function variablesGet()
    {
        return $this->variables;
    }

    public function basicAuth()
    {
        if ((!$config_auth = $this->variableGet('basic_auth')) || (php_sapi_name() == 'cli')) {
            return;
        }

        if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die('Please auth to use API.');
        }
        elseif ($config_auth['username'] !== $_SERVER['PHP_AUTH_USER'] && $config_auth['password'] !== $_SERVER['PHP_AUTH_PW']) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die('Wrong username/password');
        }
    }

}
