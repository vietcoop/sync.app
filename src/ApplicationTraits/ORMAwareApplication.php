<?php

namespace GoCatalyze\SyncApp\ApplicationTraits;

use DateTime;
use Doctrine\Common\Cache\ArrayCache as Cache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GoCatalyze\SyncApp\Entity\QueueJobEntity;
use RuntimeException;

trait ORMAwareApplication
{

    /**
     * @var EntityManagerInterface[]
     */
    protected $em;

    /**
     * Entity directories.
     *
     * @var string[]
     */
    protected $entity_dirs = [];

    /**
     * Setter for em property.
     *
     * @param EntityManagerInterface $em
     */
    public function setEntityManager($name, EntityManagerInterface $em)
    {
        $this->em[$name] = $em;
    }

    /**
     * Add entity directory.
     *
     * @param string $dir
     */
    public function addEntityDir($dir)
    {
        $this->entity_dirs[] = $dir;
    }

    /**
     * Doctrine ORM entities's path.
     *
     * @return string[]
     */
    protected function getEntityDirs()
    {
        return array_merge([dirname(__DIR__) . '/Entity/'], $this->entity_dirs);
    }

    /**
     * Getter for em property.
     *
     * @todo Implement me, configuration aware.
     * @return EntityManager
     */
    public function getEntitiyManager($name = 'default')
    {
        if (is_null($this->em[$name])) {
            $this->setEntityManager($name, $this->generateDefaultEntityManager($name));
        }

        return $this->em[$name];
    }

    /**
     * Default entity manager can be customised by override this method.
     *
     * @return EntityManagerInterface
     */
    public function generateDefaultEntityManager($name)
    {
        $config = new Configuration();
        $cache = new Cache();

        $config->setMetadataDriverImpl($config->newDefaultAnnotationDriver($this->getEntityDirs()));
        $config->setQueryCacheImpl($cache);
        $config->setMetadataCacheImpl($cache);
        $config->setProxyDir($this->app_root . '/files/tmp/entity_proxy');
        $config->setProxyNamespace('EntityProxy');
        $config->setAutoGenerateProxyClasses(true);

        // Connection information
        if ($connections = $this->variableGet('database')) {
            if (!isset($connections[$name])) {
                throw new RuntimeException(sprintf("Connection '%s' is not configured.", $name));
            }
            $connection = $connections[$name];
        }

        return EntityManager::create($connection, $config);
    }

    /**
     * Helper method to get a queue job.
     *
     * @param int|null $id
     * @return QueueJobEntity
     */
    public function getQueueJob($id = null, $class_name = 'GoCatalyze\SyncApp\Entity\QueueJobEntity', $mapping_id = null)
    {
        $repos = $this->getEntitiyManager()->getRepository($class_name);

        if (null === $id) {
            $q = $repos->createQueryBuilder('Job');
            $q->andWhere($q->expr()->in('Job.state', [QueueJobEntity::STATE_NEW, QueueJobEntity::STATE_PENDING, QueueJobEntity::STATE_TERMINATED]));
            $q->andWhere($q->expr()->lte('Job.executeAfter', ':datetime'));

            if (null !== $mapping_id) {
                $q->andWhere($q->expr()->eq('command', ':cmd'));
                $q->setParameter(':cmd', "syncEntity:Mapping:{$mapping_id}");
            }

            $q->addOrderBy('Job.priority', 'DESC');
            $q->addOrderBy('Job.closedAt', 'ASC');
            $q->setParameter(':datetime', new DateTime('- 2 seconds'), \Doctrine\DBAL\Types\Type::DATETIME);
            $q->setMaxResults(1);

            if ($results = $q->getQuery()->execute()) {
                return reset($results);
            }

            return null;
        }

        return $repos->find($id);
    }

    /**
     * Count active jobs in queue.
     *
     * @param string $cmd
     * @param string $class_name
     * @return int
     */
    public function countQueueJobs($cmd = '', $class_name = 'GoCatalyze\SyncApp\Entity\QueueJobEntity')
    {
        $count = $this->getEntitiyManager()->getRepository($class_name)->createQueryBuilder('Job');
        $count->select($count->expr()->countDistinct('Job.id'));
        $count->andWhere($count->expr()->in('Job.state', [QueueJobEntity::STATE_NEW, QueueJobEntity::STATE_PENDING, QueueJobEntity::STATE_TERMINATED]));
        if (!empty($cmd)) {
            $count->andWhere($count->expr()->eq('Job.command', ':cmd'));
            $count->setParameter(':cmd', $cmd);
        }
        return $count->getQuery()->execute()[0][1];
    }

    /**
     *
     * @param int $limit
     * @param int $offset
     * @param string $class_name
     */
    public function getQueueJobs($limit = 50, $offset = 0, $class_name = 'GoCatalyze\SyncApp\Entity\QueueJobEntity', $cmd = '', $admin = false)
    {
        $count = $this->getEntitiyManager()->getRepository($class_name)->createQueryBuilder('Job');
        $count->select($count->expr()->countDistinct('Job.id'));

        if (!$admin) {
            $count->andWhere($count->expr()->in('Job.state', [QueueJobEntity::STATE_NEW, QueueJobEntity::STATE_PENDING, QueueJobEntity::STATE_TERMINATED]));
        }

        if (!empty($cmd)) {
            $count->andWhere($count->expr()->eq('Job.command', ':cmd'));
            $count->setParameter(':cmd', $cmd);
        }

        $q = clone $count;
        $q->select('Job.id, Job.priority, Job.state, Job.createdAt, Job.executeAfter');
        $q->addOrderBy('Job.priority', 'DESC');
        $q->addOrderBy('Job.closedAt', 'ASC');
        $q->setFirstResult($offset);
        $q->setMaxResults($limit);

        return [
            'total'   => $count->getQuery()->execute()[0][1],
            'limit'   => $limit,
            'offset'  => $offset,
            'results' => $q->getQuery()->execute(),
        ];
    }

}
