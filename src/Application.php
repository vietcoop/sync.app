<?php

namespace GoCatalyze\SyncApp;

use GoCatalyze\SyncCenter\BaseManager;
use GoCatalyze\SyncCenter\ManagerInterface;

class Application
{

    use \GoCatalyze\SyncApp\ApplicationTraits\BaseApplication,
        \GoCatalyze\SyncApp\ApplicationTraits\RouteAwareApplication,
        \GoCatalyze\SyncApp\ApplicationTraits\ORMAwareApplication,
        \GoCatalyze\SyncApp\ApplicationTraits\ValidatorAwareApplication,
        \Psr\Log\LoggerAwareTrait;

    /**
     * Sync manager.
     *
     * @var ManagerInterface
     */
    protected $sync_manager;

    /**
     * Getter for sync_manager property.
     *
     * @return ManagerInterface
     */
    public function getSyncManager()
    {
        if (null === $this->sync_manager) {
            $this->sync_manager = new BaseManager();

            if ($extensions = $this->variableGet('sync_center.extensions')) {
                foreach ($extensions as $class_name) {
                    $ext = new $class_name($this->sync_manager);
                    $this->sync_manager->registerExtension($ext);
                }
            }
        }

        return $this->sync_manager;
    }

}
