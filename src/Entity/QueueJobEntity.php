<?php

namespace GoCatalyze\SyncApp\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use LogicException;

/**
 * Queue service can be a remote service, for example Amazon SQS, which may not
 * support our needing API on jobqueue: update, reschedule, log output, track
 * retries, …
 *
 * That's why we need this entity, which help us monitor sync-attempts independent
 * on queue service.
 *
 * @Entity
 * @Table(
 *    name="jobqueue",
 *    indexes={
 *      @index(name="idx_command", columns={"command"}),
 *      @index(name="idx_md5", columns={"md5"})
 *    },
 *    uniqueConstraints={
 *      @UniqueConstraint(name="unq_md5", columns={"md5"})
 *    }
 * )
 */
class QueueJobEntity
{

    use \AndyTruong\Serializer\SerializableTrait;

    /** State if job is inserted, but not yet ready to be started. */
    const STATE_NEW = 'new';

    /**
     * State if job is inserted, and might be started.
     *
     * It is important to note that this does not automatically mean that all
     * jobs of this state can actually be started, but you have to check
     * isStartable() to be absolutely sure.
     *
     * In contrast to NEW, jobs of this state at least might be started,
     * while jobs of state NEW never are allowed to be started.
     */
    const STATE_PENDING = 'pending';

    /** State if job was never started, and will never be started. */
    const STATE_CANCELED = 'canceled';

    /** State if job was started and has not exited, yet. */
    const STATE_RUNNING = 'running';

    /** State if job exists with a successful exit code. */
    const STATE_FINISHED = 'finished';

    /** State if job exits with a non-successful exit code. */
    const STATE_FAILED = 'failed';

    /** State if job exceeds its configured maximum runtime. */
    const STATE_TERMINATED = 'terminated';

    /**
     * State if an error occurs in the runner command.
     *
     * The runner command is the command that actually launches the individual
     * jobs. If instead an error occurs in the job command, this will result
     * in a state of FAILED.
     */
    const STATE_INCOMPLETE = 'incomplete';

    /**
     * State if an error occurs in the runner command.
     *
     * The runner command is the command that actually launches the individual
     * jobs. If instead an error occurs in the job command, this will result
     * in a state of FAILED.
     */
    const DEFAULT_QUEUE = 'default';
    const MAX_QUEUE_LENGTH = 50;
    const PRIORITY_LOW = -5;
    const PRIORITY_DEFAULT = 0;
    const PRIORITY_HIGH = 5;

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /** @Column(type="string", length=15) */
    private $state;

    /** @Column(type="string", length=GoCatalyze\SyncApp\Entity\QueueJobEntity::MAX_QUEUE_LENGTH) */
    private $queue;

    /** @Column(type="smallint") */
    private $priority = 0;

    /** @Column(type="datetime", name="createdAt") */
    private $createdAt;

    /** @Column(type="datetime", name="startedAt", nullable=true) */
    private $startedAt;

    /** @Column(type="datetime", name="checkedAt", nullable=true) */
    private $checkedAt;

    /** @Column(type="datetime", name="executeAfter", nullable=true) */
    private $executeAfter;

    /** @Column(type="datetime", name="closedAt", nullable=true) */
    private $closedAt;

    /** @Column(type="string") */
    private $command;

    /** @Column(type="json_array") */
    private $args;

    /** @Column(type="text", nullable=true) */
    private $output;

    /** @Column(type="text", name="errorOutput", nullable=true) */
    private $errorOutput;

    /** @Column(type="smallint", name="exitCode", nullable=true, options={"unsigned": true}) */
    private $exitCode;

    /** @Column(type="smallint", name="maxRuntime", options={"unsigned": true}) */
    private $maxRuntime = 0;

    /** @Column(type="smallint", name="maxRetries", options={"unsigned": true}) */
    private $maxRetries = 0;

    /**
     * @ManyToOne(targetEntity="QueueJobEntity", inversedBy="retryJobs")
     * @JoinColumn(name="originalJob_id", referencedColumnName="id")
     */
    private $originalJob;

    /** OneToManyy(targetEntity=QueueJobEntityy", mappedBy="originalJob", cascade={"persist", "remove", "detach"}) */
    private $retryJobs;

    /** @Column(type="smallint", nullable=true, options={"unsigned": true}) */
    private $runtime;

    /** @Column(type="integer", name="memoryUsage", nullable=true, options={"unsigned": true}) */
    private $memoryUsage;

    /** @Column(type="integer", name="memoryUsageReal", nullable=true, options={"unsigned": true}) */
    private $memoryUsageReal;

    /**
     * To avoid job duplication, we need this.
     *
     * @Column(type="string", length=255, nullable=true)
     */
    private $md5;

    public function __construct($command, array $args = [], $confirmed = true, $queue = self::DEFAULT_QUEUE, $priority = self::PRIORITY_DEFAULT)
    {
        if (trim($queue) === '') {
            throw new \InvalidArgumentException('$queue must not be empty.');
        }
        if (strlen($queue) > self::MAX_QUEUE_LENGTH) {
            throw new \InvalidArgumentException(sprintf('The maximum queue length is %d, but got "%s" (%d chars).', self::MAX_QUEUE_LENGTH, $queue, strlen($queue)));
        }

        $this->command = $command;
        $this->args = $args;
        $this->state = $confirmed ? self::STATE_PENDING : self::STATE_NEW;
        $this->queue = $queue;
        $this->priority = $priority * -1;
        $this->createdAt = new DateTime();
        $this->executeAfter = new DateTime();
        $this->executeAfter = $this->executeAfter->modify('-1 second');
        $this->retryJobs = new ArrayCollection();
        $this->relatedEntities = new ArrayCollection();
    }

    public function __clone()
    {
        $this->state = self::STATE_PENDING;
        $this->createdAt = new DateTime();
        $this->startedAt = null;
        $this->checkedAt = null;
        $this->closedAt = null;
        $this->output = null;
        $this->errorOutput = null;
        $this->exitCode = null;
        $this->stackTrace = null;
        $this->runtime = null;
        $this->memoryUsage = null;
        $this->memoryUsageReal = null;
        $this->relatedEntities = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getPriority()
    {
        return $this->priority * -1;
    }

    public function isStartable()
    {
        foreach ($this->dependencies as $dep) {
            if ($dep->getState() !== self::STATE_FINISHED) {
                return false;
            }
        }

        return true;
    }

    public function setState($newState)
    {
        if ($newState === $this->state) {
            return;
        }

        switch ($this->state) {
            case self::STATE_NEW:
                if (!in_array($newState, [self::STATE_PENDING, self::STATE_CANCELED], true)) {
                    throw new InvalidStateTransitionException($this, $newState, [self::STATE_PENDING, self::STATE_CANCELED]);
                }

                if (self::STATE_CANCELED === $newState) {
                    $this->closedAt = new DateTime();
                }

                break;

            case self::STATE_PENDING:
                if (!in_array($newState, [self::STATE_RUNNING, self::STATE_CANCELED], true)) {
                    throw new \RuntimeException('Invalid state transition: ' . $newState);
                }

                if ($newState === self::STATE_RUNNING) {
                    $this->startedAt = new DateTime();
                    $this->checkedAt = new DateTime();
                }
                else if ($newState === self::STATE_CANCELED) {
                    $this->closedAt = new DateTime();
                }

                break;

            case self::STATE_RUNNING:
                if (!in_array($newState, [self::STATE_FINISHED, self::STATE_FAILED, self::STATE_TERMINATED, self::STATE_INCOMPLETE])) {
                    throw new InvalidStateTransitionException($this, $newState, [self::STATE_FINISHED, self::STATE_FAILED, self::STATE_TERMINATED, self::STATE_INCOMPLETE]);
                }

                $this->closedAt = new DateTime();

                break;

            case self::STATE_FINISHED:
            case self::STATE_FAILED:
            case self::STATE_TERMINATED:
            case self::STATE_INCOMPLETE:
                throw new InvalidStateTransitionException($this, $newState);

            default:
                throw new LogicException('The previous cases were exhaustive. Unknown state: ' . $this->state);
        }

        $this->state = $newState;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getClosedAt()
    {
        if (null !== $this->closedAt) {
            return $this->closedAt;
        }

        return new \DateTime();
    }

    public function getExecuteAfter()
    {
        return $this->executeAfter;
    }

    public function setExecuteAfter(DateTime $executeAfter)
    {
        $this->executeAfter = $executeAfter;
    }

    public function getCommand()
    {
        return $this->command;
    }

    public function getArgs()
    {
        return $this->args;
    }

    public function getRelatedEntities()
    {
        return $this->relatedEntities;
    }

    public function isClosedNonSuccessful()
    {
        return self::isNonSuccessfulFinalState($this->state);
    }

    public function getRuntime()
    {
        return $this->runtime;
    }

    public function setRuntime($time)
    {
        $this->runtime = (integer) $time;
    }

    public function setMemoryUsage($memoryUsage)
    {
        $this->memoryUsage = $memoryUsage;
        return $this;
    }

    public function getMemoryUsage()
    {
        return $this->memoryUsage;
    }

    public function setMemoryUsageReal($memoryUsageReal)
    {
        $this->memoryUsageReal = $memoryUsageReal;
        return $this;
    }

    public function getMemoryUsageReal()
    {
        return $this->memoryUsageReal;
    }

    public function addOutput($output)
    {
        $this->output .= $output;
    }

    public function addErrorOutput($output)
    {
        $this->errorOutput .= $output;
    }

    public function setOutput($output)
    {
        $this->output = $output;
    }

    public function setErrorOutput($output)
    {
        $this->errorOutput = $output;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function getErrorOutput()
    {
        return $this->errorOutput;
    }

    public function setExitCode($code)
    {
        $this->exitCode = $code;
    }

    public function getExitCode()
    {
        return $this->exitCode;
    }

    public function setMaxRuntime($time)
    {
        $this->maxRuntime = (integer) $time;
    }

    public function getMaxRuntime()
    {
        return $this->maxRuntime;
    }

    public function getStartedAt()
    {
        return $this->startedAt;
    }

    public function getMaxRetries()
    {
        return $this->maxRetries;
    }

    public function setMaxRetries($tries)
    {
        $this->maxRetries = (integer) $tries;
    }

    public function isRetryAllowed()
    {
        // If no retries are allowed, we can bail out directly, and we
        // do not need to initialize the retryJobs relation.
        if (0 === $this->maxRetries) {
            return false;
        }

        return count($this->retryJobs) < $this->maxRetries;
    }

    public function getOriginalJob()
    {
        if (null === $this->originalJob) {
            return $this;
        }

        return $this->originalJob;
    }

    public function setOriginalJob(QueueJobEntity $job)
    {
        if (self::STATE_PENDING !== $this->state) {
            throw new LogicException($this . ' must be in state "PENDING".');
        }

        if (null !== $this->originalJob) {
            throw new LogicException($this . ' already has an original job set.');
        }

        $this->originalJob = $job;
    }

    public function addRetryJob(QueueJobEntity $job)
    {
        if (self::STATE_RUNNING !== $this->state) {
            throw new LogicException('Retry jobs can only be added to running jobs.');
        }

        $job->setOriginalJob($this);
        $this->retryJobs->add($job);
    }

    public function getRetryJobs()
    {
        return $this->retryJobs;
    }

    public function isRetryJob()
    {
        return null !== $this->originalJob;
    }

    public function checked()
    {
        $this->checkedAt = new DateTime();
    }

    public function getCheckedAt()
    {
        return $this->checkedAt;
    }

    public function setStackTrace($ex)
    {
        $this->stackTrace = $ex;
    }

    public function getStackTrace()
    {
        return $this->stackTrace;
    }

    public function getQueue()
    {
        return $this->queue;
    }

    public function isNew()
    {
        return self::STATE_NEW === $this->state;
    }

    public function isPending()
    {
        return self::STATE_PENDING === $this->state;
    }

    public function isCanceled()
    {
        return self::STATE_CANCELED === $this->state;
    }

    public function isRunning()
    {
        return self::STATE_RUNNING === $this->state;
    }

    public function isTerminated()
    {
        return self::STATE_TERMINATED === $this->state;
    }

    public function isFailed()
    {
        return self::STATE_FAILED === $this->state;
    }

    public function isFinished()
    {
        return self::STATE_FINISHED === $this->state;
    }

    public function isIncomplete()
    {
        return self::STATE_INCOMPLETE === $this->state;
    }

    public function setMd5($md5)
    {
        $this->md5 = $md5;
    }

    public function getMd5()
    {
        return $this->md5;
    }

    private function mightHaveStarted()
    {
        if (null === $this->id) {
            return false;
        }

        if (self::STATE_NEW === $this->state) {
            return false;
        }

        if (self::STATE_PENDING === $this->state && !$this->isStartable()) {
            return false;
        }

        return true;
    }

    /**
     * Camelizes a given string.
     *
     * @param string $string Some string
     *
     * @return string The camelized version of the string
     */
    private function camelize($string)
    {
        return preg_replace_callback('/(^|_|\.)+(.)/', function ($match) {
            return ('.' === $match[1] ? '_' : '') . strtoupper($match[2]);
        }, $string);
    }

    /**
     * Wrapper for public setters.
     *
     * @param string $pty
     * @param mixed $value
     * @return QueueJobEntity
     * @throws \RuntimeException
     */
    public function setPropertyValue($pty, $value)
    {
        $method_name = 'set' . $this->camelize($pty);
        if (method_exists($this, $method_name)) {
            return $this->{$method_name}($value);
        }

        if (\ReflectionProperty($this, $pty)->isPublic()) {
            $this->{$pty} = $value;
            return $this;
        }

        $msg = strtr('Can not write :class.:property', [':class' => __CLASS__, ':property' => $pty]);
        throw new \RuntimeException($msg);
    }

}
