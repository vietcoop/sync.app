<?php

namespace GoCatalyze\SyncApp\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity
 * @Table(name="service_instance_details")
 */
class RemoteEntityInfoEntity
{

    /**
     * Instance ID.
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Service instance.
     *
     * @Column(type="integer", nullable=false)
     */
    private $service_instance_id;

    /**
     * Remote entity type.
     *
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    private $entity_type;

    /**
     * Details of remote entity info.
     *
     * @Column(type="json_array", nullable=true)
     * @var array
     */
    private $details;

    public function getId()
    {
        return $this->id;
    }

    public function setServiceInstanceId($sid)
    {
        $this->service_instance_id = $sid;
        return $this;
    }

    public function getServiceInstanceId()
    {
        return $this->service_instance_id;
    }

    public function setEntityType($type)
    {
        $this->entity_type = $type;
        return $this;
    }

    public function getEntityType()
    {
        return $this->entity_type;
    }

    public function setDetails(array $details = [])
    {
        $this->details = $details;
        return $this;
    }

    public function getDetails()
    {
        if (isset($this->details['fields'][0])) {
            foreach ($this->details['fields'] as $i => $field) {
                $this->details['fields'][$field['name']] = $field;
                unset($this->details['fields'][$i]);
            }
        }
        return $this->details;
    }

}
