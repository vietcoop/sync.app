<?php

namespace GoCatalyze\SyncApp\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\True;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @Entity
 * @Table(name="service_instance")
 */
class ServiceInstanceEntity
{

    /**
     * Instance ID.
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Description for service instance.
     *
     * @Column(type="text")
     * @var string
     */
    private $description;

    /**
     * Name of service.
     *
     * @Column(type="string", length=255, nullable=false)
     * @var string
     */
    private $service_name;

    /**
     * Options for service instance (authentication, proxy connection, …).
     *
     * @Column(type="json_array")
     * @var array
     */
    private $options = [];

    /**
     * Getter for id property.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for description property.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Getter for service_name property.
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->service_name;
    }

    /**
     * Getter for object property.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Setter for id property.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Setter for description property.
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Setter for service_name property.
     *
     * @param string $service_name
     */
    public function setServiceName($service_name)
    {
        $this->service_name = $service_name;
    }

    /**
     * Wrapper for other setters.
     *
     * @param string $k
     * @param mixed $v
     * @throws \InvalidArgumentException
     */
    public function setPropertyValue($k, $v)
    {
        switch ($k) {
            case 'id':
                $this->setId($v);
                break;
            case 'status':
                $this->setStatus($v);
                break;
            case 'service_name':
                $this->setServiceName($v);
                break;
            case 'description':
                $this->setDescription($v);
                break;
            case 'options':
                $this->setOptions($v);
                break;
            case 'config_schema':
                // Skip
                break;
            default:
                throw new InvalidArgumentException(sprintf('%s is not a valid property name.', $k));
        }
    }

    /**
     * Setter for options property.
     *
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('service_name', new NotBlank());
        $metadata->addPropertyConstraint('description', new Length(['max' => 255]));
        $metadata->addGetterConstraint('validateProperties', new True([
            'message' => 'Invalid input type'
        ]));
    }

    public function isValidateProperties()
    {
        return (null !== $this->id && is_int($this->id)) || (null !== $this->options && is_array($this->options)) || (null !== $this->options && is_array($this->options))
        ;
    }

    /**
     * Convert object data to array.
     *
     * @return array
     */
    public function toArray()
    {
        $return = [];

        $keys = ['id', 'service_name', 'description', 'options'];

        foreach ($keys as $k) {
            if (null !== $this->{$k}) {
                $return[$k] = $this->{$k};
            }
        }

        return $return;
    }

    public static function fromArray(array $input)
    {
        $me = new static();

        foreach ($input as $k => $v) {
            $me->setPropertyValue($k, $v);
        }

        return $me;
    }

}
