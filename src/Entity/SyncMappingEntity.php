<?php

namespace GoCatalyze\SyncApp\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingItem;
use InvalidArgumentException;
use UnexpectedValueException;

/**
 * @Entity
 * @Table(name="mapping")
 */
class SyncMappingEntity
{

    /**
     * Entity ID.
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * Status — Enable or disable mapping.
     *
     * @Column(type="boolean")
     * @var boolean
     */
    private $status;

    /**
     * Priority.
     *
     * @Column(type="integer", nullable=true)
     * @var int
     */
    private $priority = 0;

    /**
     * Flag to know a mapping is being processed.
     *
     * @Column(type="boolean")
     * @var boolean
     */
    private $processing = false;

    /**
     * Date time of last updated.
     *
     * @Column(type="datetime")
     * @var DateTime
     */
    private $updated_at;

    /**
     * Date time of last runtime
     * @Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $run_at;

    /**
     * Mapping's description.
     *
     * @Column(type="text")
     * @var string
     */
    private $description;

    /**
     * Source
     *
     * @ManyToOne(targetEntity="ServiceInstanceEntity")
     * @var ServiceInstanceEntity
     */
    private $source_service_instance;

    /**
     * Source entity type.
     *
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    private $source_entity_type;

    /**
     * Source > Remote entity type.
     *
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    private $source_remote_entity_type;

    /**
     * Destination
     *
     * @ManyToOne(targetEntity="ServiceInstanceEntity")
     * @var ServiceInstanceEntity
     */
    private $destination_service_instance;

    /**
     * Destination entity type.
     *
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    private $destination_entity_type;

    /**
     * Destination > Remote entity type.
     *
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    private $destination_remote_entity_type;

    /**
     * Destination › Unique fields.
     *
     * @Column(type="json_array", nullable=true)
     * @var array
     */
    private $destination_unique_fields;

    /**
     * @Column(type="json_array")
     * @var EntityMappingItem[]
     */
    private $mapping_info = [];

    /**
     * Getter for id property.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for status property.
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Getter for processing property.
     *
     * @return bollean
     */
    public function getProcessing()
    {
        return $this->processing;
    }

    /**
     * Getter for updated_at property.
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        if (null === $this->updated_at) {
            $this->updated_at = new DateTime();
        }

        return $this->updated_at;
    }

    /**
     * Get time of last run.
     *
     * @return DateTime
     */
    public function getRunAt()
    {
        if (null === $this->run_at) {
            $this->run_at = new DateTime('- 1 year');
        }
        return $this->run_at;
    }

    /**
     * Getter for description property.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Getter for source_service_instance property.
     *
     * @return ServiceInstanceEntity
     */
    public function getSourceServiceInstance()
    {
        return $this->source_service_instance;
    }

    /**
     * Getter for destination_service_instance property.
     *
     * @return ServiceInstanceEntity
     */
    public function getDestinationServiceInstance()
    {
        return $this->destination_service_instance;
    }

    /**
     * Getter for mapping_info property.
     *
     * @return array
     */
    public function getMappingInfo()
    {
        return $this->mapping_info;
    }

    /**
     * Setter for id property.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Setter for status property.
     *
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Setter for processing property.
     *
     * @param boolean $processing
     */
    public function setProcessing($processing)
    {
        $this->processing = $processing;
    }

    /**
     * Setter for updated_at property.
     *
     * @param DateTime|string|int $datetime
     */
    public function setUpdatedAt($datetime, $format = 'Y-m-d H:i:s', $property_name = 'updated_at')
    {
        if (!$datetime instanceof DateTime) {
            if (is_string($datetime)) {
                $datetime = date_create_from_format($format, $datetime);
            }
            elseif (is_int($datetime)) {
                $datetime = date_create_from_format($format, date($format, $datetime));
            }
            else {
                throw new UnexpectedValueException('Updated time is not valid');
            }
        }

        $this->{$property_name} = $datetime;
    }

    /**
     * Setter for run_at property.
     *
     * @param DateTime|string|int $run_at
     * @param type $format
     */
    public function setRunAt($run_at, $format = 'Y-m-d H:i:s')
    {
        $this->setUpdatedAt($run_at, $format, 'run_at');
    }

    /**
     * Setter for description property.
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Setter for source_service_instance property.
     *
     * @param ServiceInstanceEntity $source_service_instance
     */
    public function setSourceServiceInstance(ServiceInstanceEntity $source_service_instance)
    {
        $this->source_service_instance = $source_service_instance;
    }

    /**
     * Setter for destination_service_instance property.
     *
     * @param ServiceInstanceEntity $destination_service_instance
     */
    public function setDestinatinationServiceInstance(ServiceInstanceEntity $destination_service_instance)
    {
        $this->destination_service_instance = $destination_service_instance;
    }

    /**
     * Setter for mapping_items property.
     *
     * @param array $info
     */
    public function setMappingInfo(array $info = [])
    {
        $this->mapping_info = $info;
    }

    /**
     * Wrapper for other setters.
     *
     * @param string $k
     * @param mixed $v
     * @throws \InvalidArgumentException
     */
    public function setPropertyValue($k, $v)
    {
        switch ($k) {
            case 'id':
                $this->setId($v);
                break;
            case 'status':
                $this->setStatus($v);
                break;
            case 'priority':
                $this->setPriority($v);
                break;
            case 'description':
                $this->setDescription($v);
                break;
            case 'source':
            case 'destination':
                foreach ($v as $kk => $vv) {
                    switch ($kk) {
                        case 'unique_fields':
                            // source does not have unique-fields option yet
                            $k === 'source' ? '…' : $this->setDestinationUniqueFields($vv);
                            break;
                        case 'entity_type':
                        case 'remote_entity_type':
                            $this->{"{$k}_{$kk}"} = $vv;
                            break;
                        case 'service_instance':
                            if (is_array($vv)) {
                                $vv = ServiceInstanceEntity::fromArray($vv);
                            }
                            $k === 'source' ? $this->setSourceServiceInstance($vv) : $this->setDestinatinationServiceInstance($vv);
                            break;
                    }
                }
                break;
            case 'processing':
                $this->setProcessing($v);
                break;
            case 'updated_at':
                $this->setUpdatedAt($v, strpos($v, 'T') ? DATE_ISO8601 : 'Y-m-d H:i:s');
                break;
            case 'mapping':
                $this->setMappingInfo($v);
                break;
            default:
                throw new InvalidArgumentException(sprintf('%s is not a valid property name.', $k));
        }
    }

    /**
     * Represent object as array.
     *
     * @return array
     */
    public function toArray()
    {
        $return = [];

        $properties = [
            'id'          => 'id',
            'status'      => 'status',
            'priority'    => 'priority',
            'processing'  => 'processing',
            'updated_at'  => 'updated_at',
            'description' => 'description',
            'source'      => [
                'service_instance'   => 'source_service_instance',
                'entity_type'        => 'source_entity_type',
                'remote_entity_type' => 'source_remote_entity_type',
            ],
            'destination' => [
                'service_instance'   => 'destination_service_instance',
                'entity_type'        => 'destination_entity_type',
                'remote_entity_type' => 'destination_remote_entity_type',
                'unique_fields'      => 'destination_unique_fields',
            ],
            'mapping'     => 'mapping_info',
        ];

        foreach ($properties as $k => $property) {
            if (is_string($property)) {
                if (null !== $this->{$property}) {
                    $return[$k] = $k === 'updated_at' ? $this->getUpdatedAt()->format('Y-m-d H:i:s') : $this->{$property};
                }
            }
            elseif (in_array($k, ['source', 'destination'])) {
                foreach ($property as $kk => $vv) {
                    $return[$k][$kk] = null;
                    if ($kk === 'service_instance') {
                        if (!is_null($this->{$vv})) {
                            $return[$k][$kk] = $this->{$vv}->toArray(false);
                        }
                    }
                    else {
                        $return[$k][$kk] = $this->{$vv};
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Create in static object from array.
     *
     * @param array $input
     */
    public static function fromArray(array $input)
    {
        $me = new static();
        foreach ($input as $k => $v) {
            $me->setPropertyValue($k, $v);
        }
        return $me;
    }

    /**
     * @return string
     */
    public function getSourceEntityType()
    {
        return $this->source_entity_type;
    }

    public function getSourceRemoteEntityType()
    {
        return $this->source_remote_entity_type;
    }

    /**
     * @return string
     */
    public function getDestinationEntityType()
    {
        return $this->destination_entity_type;
    }

    public function getDestinationRemoteEntityType()
    {
        return $this->destination_remote_entity_type;
    }

    public function getDestinationUniqueFields()
    {
        return $this->destination_unique_fields;
    }

    public function setSourceEntityType($source_entity_type)
    {
        $this->source_entity_type = $source_entity_type;
    }

    public function setSourceRemoteEntityType($source_remote_entity_type)
    {
        $this->source_remote_entity_type = $source_remote_entity_type;
    }

    public function setDestinationEntityType($destination_entity_type)
    {
        $this->destination_entity_type = $destination_entity_type;
    }

    public function setDestinationRemoteEntityType($destination_remote_entity_type)
    {
        $this->destination_remote_entity_type = $destination_remote_entity_type;
    }

    public function setDestinationUniqueFields(array $destination_unique_fields)
    {
        if (is_array($destination_unique_fields[0])) {
            foreach ($destination_unique_fields as &$_destination_unique_fields) {
                $_destination_unique_fields = array_filter($_destination_unique_fields);
            }
        }
        $this->destination_unique_fields = array_filter($destination_unique_fields);
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

}
