<?php

$conf = ['mode' => 'testing', 'debug' => false];

$conf['database']['default'] = [
    'driver' => 'pdo_sqlite',
    'user' => 'db_user',
    'password' => 'db_pwd',
    'path' => dirname(__DIR__) . '/files/syncapp_testing.sqlite',
    'memory' => true,
    'charset' => 'UTF8',
];

$conf['queue'] = [
    'backend' => 'pdo',
    'options' => [
        'connection_string' => sprintf("sqlite:%s/files/syncapp_testing.sqlite", dirname(__DIR__)),
        'db_user' => $conf['database']['default']['user'],
        'db_password' => $conf['database']['default']['password'],
        'db_table' => 'jobqueue',
        'pdo_options' => ['charset' => 'UTF8']
    ],
//    'backend' => 'amazon.sqs',
//    'options' => [ // @see PHPQueue\Backend\Aws\AmazonSQSV2::__construct()
//        'region' => 'ap-southeast-1',
//        'queue' => 'https://sqs.ap-southeast-1.amazonaws.com/769005983919/SyncAppTesting',
//        'attribute_options' => [],
//        'receiving_options' => [],
//        'sqs_options' => [
//            'key' => 'AKIAIJ7YBHWJDT4FAGIQ',
//            'secret' => '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',
//        ]
//    ]
];

return $conf;
