<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

define('GOCATALYZE_SYNCCENTER_APPLICATION_DONT_RUN', true);
$app = require_once dirname(__DIR__) . '/web/index.php';

return ConsoleRunner::createHelperSet($app->getEntitiyManager());
