Sync-App is a web service application to sync content from various remote-services.
For examples: Drupal User <> Salesforce Contact.

## Concepts:

1. Service Instance: Object where we know a remote service with full authentication.
2. Sync Mapping: Object provide informations of source/destination service instances,
    also mapping fields.
3. Remote entity: Objects on remote service instance, e.g. Node/User on Drupal.
4. Queue item: Commands to to push data changes to destination is not executed
    directly, but they should be queued, execute only when ./process resource
    is being requested.
5. Extension: Sync app can only be able to connect, fetch data, put data when
    the app has extension enabled. By default, we provides: Drupal, Salesforce,
    Twitter, Mailchimp extensions. You can follow GoCatalyze\SyncCenter\ExtensionInterface
    to define new extension.

## Install

1. Update configuration in ./config/config.php
2. Run `composer install --no-dev`
3. Install database schema: `php ./vendor/bin/doctrine.php orm:schema-tool:create`
4. On update code, you should run: `php ./vendor/bin/doctrine.php orm:schema-tool:update --force`
5. Cronjob: /mapping/fetch > Fetch most priority mapping.
6. Cronjob: /queue/process > Process most priority queue job.
8. Cronjob: /queue/cleanup > Cleanup completed, canceled, failed jobs.
